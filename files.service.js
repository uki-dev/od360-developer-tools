'use strict';

const fs = require('fs-extra');
const path = require('path');
const uuid = require('uuid').v4;
const obfuscator = require('javascript-obfuscator');
const puppeteer = require('puppeteer');
const archiver = require('archiver');
const drivelist = require('drivelist');

const PROJECT_DB = "./project-db.json";

class FilesService {

    static async listDrives() {
        return new Promise((resolve, reject) => {
            drivelist.list((error, drives) => {
                if (error) {
                    reject(error);
                }
                else {
                    let paths = new Array();
                    drives.forEach((d) => {
                        d.mountpoints.forEach((m) => {
                            paths.push(m.path);
                        });
                    });
                    resolve(paths);
                }
            });
        });
    }

    static listFiles(directoryPath) {
        directoryPath = FilesService.getOSPath(directoryPath);
        return new Promise((resolve) => {
            FilesService._fileWalker(directoryPath, function (err, data) {
                if (err) {
                    console.log(err);
                    resolve([]);
                }
                else {
                    resolve(
                        data.map((filePath) => {
                            return FilesService._formatPath(filePath, directoryPath);
                        })
                    );
                }
            });
        });
    }

    static listSubDirectories(directoryPath) {
        return new Promise((resolve) => {
            directoryPath = FilesService.getOSPath(directoryPath);
            FilesService._readSubDirectories(directoryPath, function(err, data) {
                if (err) {
                    console.log(err);
                    resolve([]);
                }
                else {
                    resolve(
                        data.map((dirPath) => {
                            return FilesService._formatPath(dirPath, directoryPath);
                        })
                        .filter((dirPath) => {
                            return (dirPath[0] !== ".");
                        })
                    );
                }
            });
        }); 
    }

    static _formatPath(p, dirPath) {
        p = p.replace(dirPath, "");
        p = p.replace(/[A-Za-z]\:/g, "");
        p = FilesService.getOSPath(p);
        if (p[0] === "/" || p[0] === "\\") {
            p = p.substr(1);
        }
        return p;
    }

    static async listProjects() {
        try {
            await FilesService._createProjectDatabase();
            const projects = await fs.readJSON(PROJECT_DB);
            return projects;
        }
        catch (err) {
            console.log(err);
            return null;
        }
    }

    static async createProject(projectInfo) {
        try {
            await FilesService._createProjectDatabase();
            let projects = await FilesService.listProjects();
            const projectIndex = projects.findIndex((p) => p.directoryPath === projectInfo.directoryPath);
            if (projectIndex === -1) {
                projectInfo.id = uuid();
                projectInfo.createdAt = new Date();
                projects.push(projectInfo);
            }
            else {
                projectInfo.createdAt = new Date();
                projects[projectIndex] = projectInfo;
            }
            await fs.writeJSON(PROJECT_DB, projects);
            return true;
        }
        catch (err) {
            console.log(err);
            return false;
        }
    }

    static async deleteProject(id) {
        try {
            await FilesService._createProjectDatabase();
            let projects = await FilesService.listProjects();
            const index = projects.findIndex((p) => p.id === id);
            projects.splice(index, 1);
            await fs.writeJSON(PROJECT_DB, projects);
            return true;
        }
        catch (err) {
            console.log(err);
            return false;
        }
    }

    static async readManifest(directoryPath) {
        try {
            const manifest = await fs.readJSON(path.join(directoryPath, "od-manifest.json"));
            return manifest;
        }
        catch (err) {
            return null;
        }
    }

    static async copyProject(directoryPath, currentBuildDirectoryPath, manifest) {
        try {
            if (currentBuildDirectoryPath) {
                const exists = await fs.pathExists(currentBuildDirectoryPath);
                if (exists) {
                    console.log("removing directory: " + currentBuildDirectoryPath);
                    await fs.remove(currentBuildDirectoryPath);
                }
            }
            let buildDirectoryPath = path.join(__dirname, "projects", "od360-build-" + uuid());
            console.log(`copying ${directoryPath} to ${buildDirectoryPath}`);
            await fs.copy(directoryPath, buildDirectoryPath);
            await fs.writeJSON(path.join(directoryPath, "od-manifest.json"), manifest);
            return buildDirectoryPath;
        }
        catch (err) {
            console.log(err);
            return "";
        }
    }

    static async obfuscate(manifest, buildDirectoryPath) {
        try {
            if (manifest.app && manifest.app.tag && manifest.app.bootstrapModule) {
                manifest.app.tag = await FilesService._obfuscateFile(buildDirectoryPath, manifest.app.bootstrapModule, manifest.app.tag);
            }
            else {
                manifest.app = null;
            }
            for (let i = 0; i < manifest.widgets.length; ++i) {
                manifest.widgets[i].tag = await FilesService._obfuscateFile(buildDirectoryPath, manifest.widgets[i].bootstrapModule, manifest.widgets[i].tag);
            }
            await fs.writeJSON(path.join(buildDirectoryPath, "od-manifest.json"), manifest);
            console.log("------------------------------------");
            return manifest;
        }
        catch (err) {
            console.log(err);
            return null;
        }
    }

    static async checkShadowDom(buildDirectoryPath, manifest) {
        try {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            let idTracker = new Array();
            let content = "";
            if (manifest.app) {
                await page.addScriptTag({ path: path.join(buildDirectoryPath, manifest.app.bootstrapModule) });
                const id = FilesService._stripNumbersAndDashes(uuid());
                idTracker.push(id);
                content += `<${manifest.app.tag} id="${id}"></${manifest.app.tag}>`;
            }
            for (let i = 0; i < manifest.widgets.length; ++i) {
                await page.addScriptTag({ path: path.join(buildDirectoryPath, manifest.widgets[i].bootstrapModule) });
                const id = FilesService._stripNumbersAndDashes(uuid());
                idTracker.push(id);
                content += `<${manifest.widgets[i].tag} id="${id}"></${manifest.widgets[i].tag}>`;
            }
            await page.setContent(content, { waitUntil: 'domcontentloaded' });
            for (let i = 0; i < idTracker.length; ++i) {
                await page.waitForSelector(`#${idTracker[i]}`);
                const innerHTML = await page.$eval('#' + idTracker[i], (element) => {
                    return element.innerHTML
                });
                if (innerHTML) {
                    console.log("Shadow DOM Check Failed.");
                    console.log(innerHTML);
                    return false;
                }
            }
            return true;
        }
        catch (err) {
            console.log(err);
            return false;
        }
    }

    static zipProject(buildDirectoryPath) {
        return new Promise((resolve) => {
            console.log("Zipping project directory...");
            let outputFile = path.join(__dirname, "builds", "od360-build-" + uuid() + ".zip");
            fs.createFileSync(outputFile);
            let output = fs.createWriteStream(outputFile);
            let archive = archiver('zip');

            output.on('error', (err) => {
                console.log(err);
                resolve(null);
            });

            output.on('finish', () => {
                console.log("Zip complete: " + outputFile);
                resolve(outputFile);
            });

            archive.pipe(output);
            archive.directory(buildDirectoryPath, false);
            archive.finalize();
        });
    }

    static async _createProjectDatabase() {
        try {
            const exists = await fs.pathExists(PROJECT_DB);
            if (exists) {
                await fs.readJSON(PROJECT_DB);
            }
            else {
                await fs.writeJSON(PROJECT_DB, []);
            }
        }
        catch (err) {
            console.log(err);
            await fs.writeJSON(PROJECT_DB, []);
        }
    }

    static async _obfuscateFile(buildDirectoryPath, bootstrapModule, customElementTag) {
        const filePath = path.join(buildDirectoryPath, bootstrapModule);
        console.log("obfuscating: " + filePath);
        let contents = await fs.readFile(filePath, 'utf8');
        const elementRegExp = new RegExp(customElementTag, 'g');
        const webpackRegExp = new RegExp('window.webpackJsonp', 'gi');
        const newTag = "od" + uuid();
        const newWebpack = "window." + FilesService._stripNumbersAndDashes(uuid());
        //const newTag = customElementTag;
        let matches = contents.match(elementRegExp);
        if (!matches) {
            throw new Error();
        }
        contents = contents.replace(elementRegExp, newTag);
        contents = contents.replace(webpackRegExp, newWebpack);
        //contents = await FilesService._obfuscateContents(contents);
        //contents = `(function(){${contents}}())`;
        await fs.writeFile(filePath, contents);
        return newTag;
    }

    static async _obfuscateContents(contents) {
        const obfuscationResult = obfuscator.obfuscate(
            contents,
            {
                compact: true,
                controlFlowFlattening: false,
                deadCodeInjection: false,
                debugProtection: false,
                debugProtectionInterval: false,
                disableConsoleOutput: false,
                identifierNamesGenerator: 'hexadecimal',
                log: false,
                renameGlobals: true,
                rotateStringArray: true,
                selfDefending: false,
                stringArray: true,
                stringArrayEncoding: true,
                stringArrayThreshold: 0.75,
                unicodeEscapeSequence: false
                //identifiersPrefix: FilesService._stripNumbersAndDashes(uuid())
            }
        );
        return obfuscationResult.getObfuscatedCode();
    }

    static _stripNumbersAndDashes(value) {
        return value.replace(/[0-9\-]/g, "");
    }

    static _readSubDirectories(dir, done) {
        dir = FilesService.getOSPath(dir);
        let results = [];
        fs.readdir(dir, (err, list) => {
            if (err) {
                return done(err);
            }
            var pending = list.length;
            if (!pending) {
                return done(null, results);
            }
            list.forEach((file) => {
                file = path.resolve(dir, file);
                fs.stat(file, (err, stat) => {
                    if (stat && stat.isDirectory()) {
                        results.push(file);
                    }
                    if (!--pending) {
                        done(null, results);
                    }
                });
            });
        });
    }

    static _fileWalker(dir, done) {
        dir = FilesService.getOSPath(dir);
        let results = [];
        fs.readdir(dir, (err, list) => {
            if (err) {
                return done(err);
            }
            var pending = list.length;
            if (!pending) {
                return done(null, results);
            }
            list.forEach((file) => {
                file = path.resolve(dir, file);
                fs.stat(file, (err, stat) => {
                    // If directory, execute a recursive call
                    if (stat && stat.isDirectory()) {
                        FilesService._fileWalker(file, (err, res) => {
                            results = results.concat(res);
                            if (!--pending) {
                                done(null, results);
                            }
                        });
                    }
                    else {
                        results.push(file);
                        if (!--pending) {
                            done(null, results);
                        }
                    }
                });
            });
        });
    };

    static getOSPath(p) {
        return path.normalize(p);
    }


}

module.exports = FilesService;