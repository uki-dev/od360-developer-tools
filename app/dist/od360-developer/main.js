(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/api-call-builder/api-call-builder.component.html":
/*!******************************************************************!*\
  !*** ./src/app/api-call-builder/api-call-builder.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-grid-list cols=\"1\" rowHeight=\"65px\">\n  <mat-grid-tile>\n      <h2>Configure API Calls</h2>\n    <button mat-icon-button color=\"secondary\" class=\"ml-2\" (click)=\"addApiCall()\" matTooltip=\"Add API Call\">\n      <mat-icon>add</mat-icon>\n    </button>\n  </mat-grid-tile>\n</mat-grid-list>\n  \n<mat-card *ngFor=\"let apiCall of apiCalls; let i = index\">\n  <mat-list>\n    <mat-list-item>\n      <button mat-icon-button matTooltip=\"Remove API Call\" (click)=\"removeApiCall(i)\" class=\"closeButton opacity-half\">\n        <mat-icon>clear</mat-icon>\n      </button>\n    </mat-list-item>\n\n    <mat-list-item class=\"mb-4\">\n      <mat-form-field>\n        <input matInput placeholder=\"URL\" [(ngModel)]=\"apiCall.url\" required>\n      </mat-form-field>\n    </mat-list-item>\n\n    <mat-list-item class=\"mb-4\">\n      <mat-form-field>\n        <mat-label>Verb</mat-label>\n        <mat-select [(value)]=\"apiCall.verb\" required>\n          <mat-option [value]=\"'GET'\">\n            GET\n          </mat-option>\n          <mat-option [value]=\"'POST'\">\n            POST\n          </mat-option>\n          <mat-option [value]=\"'PUT'\">\n            PUT\n          </mat-option>\n          <mat-option [value]=\"'PATCH'\">\n            PATCH\n          </mat-option>\n          <mat-option [value]=\"'DELETE'\">\n            DELETE\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </mat-list-item>\n  </mat-list>\n\n</mat-card>\n\n<div>\n  <button mat-button matStepperPrevious class=\"mr-1\">Back</button>\n  <button mat-raised-button color=\"primary\" (click)=\"submit()\">Next</button>\n</div>"

/***/ }),

/***/ "./src/app/api-call-builder/api-call-builder.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/api-call-builder/api-call-builder.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-card {\n  display: inline-block;\n  width: calc(33% - 80px);\n  margin: 0 40px 40px 0; }\n\n.closeButton {\n  position: absolute;\n  top: 0;\n  right: 8px; }\n\nmat-card mat-form-field {\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2phbWVzL0RvY3VtZW50cy9VS0kvUmVwb3NpdG9yaWVzL0RldmVsb3Blci9vZDM2MC1kZXZlbG9wZXIvc3JjL2FwcC9hcGktY2FsbC1idWlsZGVyL2FwaS1jYWxsLWJ1aWxkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBcUI7RUFDckIsdUJBQXVCO0VBQ3ZCLHFCQUFxQixFQUFBOztBQUd6QjtFQUNJLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sVUFBVSxFQUFBOztBQUlkO0VBQ0ksV0FBVyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvYXBpLWNhbGwtYnVpbGRlci9hcGktY2FsbC1idWlsZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC1jYXJkIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IGNhbGMoMzMlIC0gODBweCk7XG4gICAgbWFyZ2luOiAwIDQwcHggNDBweCAwO1xufVxuXG4uY2xvc2VCdXR0b24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgcmlnaHQ6IDhweDtcbn1cblxuXG5tYXQtY2FyZCBtYXQtZm9ybS1maWVsZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/api-call-builder/api-call-builder.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/api-call-builder/api-call-builder.component.ts ***!
  \****************************************************************/
/*! exports provided: ApiCallBuilderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiCallBuilderComponent", function() { return ApiCallBuilderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");



var ApiCallBuilderComponent = /** @class */ (function () {
    function ApiCallBuilderComponent(ptSvc) {
        this.ptSvc = ptSvc;
        this.apiCalls = new Array();
        this.submitted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ApiCallBuilderComponent.prototype.ngOnInit = function () {
        this.subscribeToManifestUpdates();
    };
    ApiCallBuilderComponent.prototype.ngOnDestroy = function () {
        this.manifestSub.unsubscribe();
    };
    ApiCallBuilderComponent.prototype.addApiCall = function () {
        this.apiCalls.push({
            verb: "GET",
            url: "http://api-call.com/path/{$}"
        });
    };
    ApiCallBuilderComponent.prototype.removeApiCall = function (index) {
        this.apiCalls.splice(index, 1);
    };
    ApiCallBuilderComponent.prototype.submit = function () {
        this.ptSvc.manifest.apiCalls = this.apiCalls;
        this.submitted.emit();
    };
    ApiCallBuilderComponent.prototype.subscribeToManifestUpdates = function () {
        var _this = this;
        this.manifestSub = this.ptSvc.manifestSub.subscribe(function (manifest) {
            _this.apiCalls = Object.assign([], manifest.apiCalls);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ApiCallBuilderComponent.prototype, "submitted", void 0);
    ApiCallBuilderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-api-call-builder',
            template: __webpack_require__(/*! ./api-call-builder.component.html */ "./src/app/api-call-builder/api-call-builder.component.html"),
            styles: [__webpack_require__(/*! ./api-call-builder.component.scss */ "./src/app/api-call-builder/api-call-builder.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_project_tracker_service__WEBPACK_IMPORTED_MODULE_2__["ProjectTrackerService"]])
    ], ApiCallBuilderComponent);
    return ApiCallBuilderComponent;
}());



/***/ }),

/***/ "./src/app/app-info-builder/app-info-builder.component.html":
/*!******************************************************************!*\
  !*** ./src/app/app-info-builder/app-info-builder.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>Configure Application</h2>\n\n<mat-grid-list cols=\"2\" rowHeight=\"250px\">\n  <mat-grid-tile>\n    <mat-list>\n      <mat-list-item class=\"mb-2\">\n        <mat-form-field>\n          <input matInput placeholder=\"Title\" [(ngModel)]=\"manifest.title\" required>\n        </mat-form-field>\n      </mat-list-item>\n\n      <mat-list-item class=\"mb-4\">\n          <mat-form-field>\n            <input matInput placeholder=\"Version\" [(ngModel)]=\"manifest.version\" required>\n          </mat-form-field>\n        </mat-list-item>\n    \n      <mat-list-item class=\"mb-4\">\n        <mat-form-field>\n          <textarea matInput [(ngModel)]=\"manifest.description\" placeholder=\"App Description\" matTextareaAutosize matAutosizeMinRows=2 matAutosizeMaxRows=2 required></textarea>\n        </mat-form-field>\n      </mat-list-item>\n  \n    </mat-list>\n  </mat-grid-tile>\n\n  <mat-grid-tile>\n    <mat-list>    \n      <mat-list-item class=\"mt-4 mb-4\">\n        <mat-form-field>\n          <mat-label>Icon Image</mat-label>\n          <mat-select [(value)]=\"manifest.iconImage\" required>\n            <mat-option *ngFor=\"let file of imageFiles\" [value]=\"file\">\n              {{file}}\n            </mat-option>\n          </mat-select>\n          <mat-hint align=\"start\">Asset should be a 1x1 image.</mat-hint>\n        </mat-form-field>\n      </mat-list-item>\n    \n      <mat-list-item class=\"mb-2\">\n        <mat-form-field>\n          <input matInput placeholder=\"Permissions\" [ngModel]=\"permissions\" (keyup)=\"permissionsChanged($event)\">\n          <mat-hint align=\"start\">Separate values with commas.</mat-hint>\n        </mat-form-field>\n      </mat-list-item>\n    </mat-list>\n  </mat-grid-tile>\n</mat-grid-list>\n\n<div>\n  <button mat-button matStepperPrevious class=\"mr-1 mt-3\">Back</button>\n  <button mat-raised-button color=\"primary\" class=\"mt-3\" (click)=\"submit()\">Next</button>\n</div>\n"

/***/ }),

/***/ "./src/app/app-info-builder/app-info-builder.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/app-info-builder/app-info-builder.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC1pbmZvLWJ1aWxkZXIvYXBwLWluZm8tYnVpbGRlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app-info-builder/app-info-builder.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/app-info-builder/app-info-builder.component.ts ***!
  \****************************************************************/
/*! exports provided: AppInfoBuilderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppInfoBuilderComponent", function() { return AppInfoBuilderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util_file_mapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../util/file-mapper */ "./src/app/util/file-mapper.ts");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");




var AppInfoBuilderComponent = /** @class */ (function () {
    function AppInfoBuilderComponent(ptSvc) {
        this.ptSvc = ptSvc;
        this.submitted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    AppInfoBuilderComponent.prototype.ngOnInit = function () {
        this.subscribeToFileUpdates();
        this.subscribeToManifestUpdates();
    };
    AppInfoBuilderComponent.prototype.ngOnDestroy = function () {
        this.filesSub.unsubscribe();
        this.manifestSub.unsubscribe();
    };
    AppInfoBuilderComponent.prototype.permissionsChanged = function ($event) {
        var value = $event.currentTarget.value;
        if (value.length) {
            this.manifest.permissions = value.split(',').map(function (p) {
                return p.trim().replace(/[^A-Za-z\-]/g, "");
            });
        }
        else {
            this.manifest.permissions = [];
        }
    };
    AppInfoBuilderComponent.prototype.submit = function () {
        this.submitted.emit();
    };
    AppInfoBuilderComponent.prototype.subscribeToFileUpdates = function () {
        var _this = this;
        this.filesSub = this.ptSvc.filesSub.subscribe(function (files) {
            _this.imageFiles = _util_file_mapper__WEBPACK_IMPORTED_MODULE_2__["FileMapper"].mapImageFiles(files);
        });
    };
    AppInfoBuilderComponent.prototype.subscribeToManifestUpdates = function () {
        var _this = this;
        this.manifestSub = this.ptSvc.manifestSub.subscribe(function (manifest) {
            _this.manifest = manifest;
            _this.permissions = manifest.permissions.join(',');
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], AppInfoBuilderComponent.prototype, "submitted", void 0);
    AppInfoBuilderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-app-info-builder',
            template: __webpack_require__(/*! ./app-info-builder.component.html */ "./src/app/app-info-builder/app-info-builder.component.html"),
            styles: [__webpack_require__(/*! ./app-info-builder.component.scss */ "./src/app/app-info-builder/app-info-builder.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_project_tracker_service__WEBPACK_IMPORTED_MODULE_3__["ProjectTrackerService"]])
    ], AppInfoBuilderComponent);
    return AppInfoBuilderComponent;
}());



/***/ }),

/***/ "./src/app/app-renderers/app-renderers.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/app-renderers/app-renderers.module.ts ***!
  \*******************************************************/
/*! exports provided: AppRenderersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRenderersModule", function() { return AppRenderersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _micro_app_renderer_micro_app_renderer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./micro-app-renderer/micro-app-renderer.component */ "./src/app/app-renderers/micro-app-renderer/micro-app-renderer.component.ts");
/* harmony import */ var _widget_renderer_widget_renderer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./widget-renderer/widget-renderer.component */ "./src/app/app-renderers/widget-renderer/widget-renderer.component.ts");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");







var AppRenderersModule = /** @class */ (function () {
    function AppRenderersModule() {
    }
    AppRenderersModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _micro_app_renderer_micro_app_renderer_component__WEBPACK_IMPORTED_MODULE_3__["MicroAppRendererComponent"],
                _widget_renderer_widget_renderer_component__WEBPACK_IMPORTED_MODULE_4__["WidgetRendererComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_5__["MatButtonModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"]
            ],
            exports: [
                _micro_app_renderer_micro_app_renderer_component__WEBPACK_IMPORTED_MODULE_3__["MicroAppRendererComponent"],
                _widget_renderer_widget_renderer_component__WEBPACK_IMPORTED_MODULE_4__["WidgetRendererComponent"]
            ]
        })
    ], AppRenderersModule);
    return AppRenderersModule;
}());



/***/ }),

/***/ "./src/app/app-renderers/micro-app-renderer/micro-app-renderer.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/app-renderers/micro-app-renderer/micro-app-renderer.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div [id]=\"containerId\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/app-renderers/micro-app-renderer/micro-app-renderer.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/app-renderers/micro-app-renderer/micro-app-renderer.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC1yZW5kZXJlcnMvbWljcm8tYXBwLXJlbmRlcmVyL21pY3JvLWFwcC1yZW5kZXJlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app-renderers/micro-app-renderer/micro-app-renderer.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/app-renderers/micro-app-renderer/micro-app-renderer.component.ts ***!
  \**********************************************************************************/
/*! exports provided: MicroAppRendererComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MicroAppRendererComponent", function() { return MicroAppRendererComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _renderer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../renderer */ "./src/app/app-renderers/renderer.ts");
/* harmony import */ var _services_http_request_controller_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/http-request-controller.service */ "./src/app/services/http-request-controller.service.ts");
/* harmony import */ var _util_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../util/constants */ "./src/app/util/constants.ts");
/* harmony import */ var _services_app_launch_request_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/app-launch-request.service */ "./src/app/services/app-launch-request.service.ts");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");
/* harmony import */ var _util_cloner__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../util/cloner */ "./src/app/util/cloner.ts");









var MicroAppRendererComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](MicroAppRendererComponent, _super);
    function MicroAppRendererComponent(ptSvc, launchReqSvc, httpControllerSvc) {
        var _this = _super.call(this) || this;
        _this.ptSvc = ptSvc;
        _this.launchReqSvc = launchReqSvc;
        _this.httpControllerSvc = httpControllerSvc;
        return _this;
    }
    Object.defineProperty(MicroAppRendererComponent.prototype, "app", {
        get: function () {
            return this._app;
        },
        set: function (app) {
            this._app = app;
            this.destroy();
            if (this.isInitialized) {
                this.load();
            }
        },
        enumerable: true,
        configurable: true
    });
    MicroAppRendererComponent.prototype.ngOnInit = function () {
        this.subscribeToUserSession();
        this.subscribeToReload();
        this.subscribeToReloadApp();
        this.subscribeToAppLaunch();
    };
    MicroAppRendererComponent.prototype.ngAfterViewInit = function () {
        this.isInitialized = true;
        if (this.app) {
            this.load();
        }
    };
    MicroAppRendererComponent.prototype.ngOnDestroy = function () {
        this.destroy();
        if (this.userSessionSub) {
            this.userSessionSub.unsubscribe();
        }
        if (this.reloadSub) {
            this.reloadSub.unsubscribe();
        }
        if (this.reloadAppSub) {
            this.reloadAppSub.unsubscribe();
        }
        if (this.appLaunchReqSub) {
            this.appLaunchReqSub.unsubscribe();
        }
    };
    MicroAppRendererComponent.prototype.subscribeToUserSession = function () {
        var _this = this;
        this.userSessionSub = this.ptSvc.userStateSub.subscribe(function (userState) {
            _this.makeCallback(_this.userStateCallback, _util_cloner__WEBPACK_IMPORTED_MODULE_7__["Cloner"].cloneObject(_this.ptSvc.userState));
        });
    };
    MicroAppRendererComponent.prototype.subscribeToReload = function () {
        var _this = this;
        this.reloadSub = this.ptSvc.reloadSub.subscribe(function () {
            _this.reload();
        });
    };
    MicroAppRendererComponent.prototype.subscribeToReloadApp = function () {
        var _this = this;
        this.reloadAppSub = this.ptSvc.reloadAppSub.subscribe(function () {
            _this.reload();
        });
    };
    MicroAppRendererComponent.prototype.load = function () {
        var _this = this;
        var container = document.getElementById(this.containerId);
        this.customElem = this.buildCustomElement(this.app.appTag);
        this.setupElementIO();
        container.appendChild(this.customElem);
        var script = this.buildScriptTag(this.ptSvc.buildDirectoryPath, this.app.appBootstrap);
        if (!this.scriptExists(script.src)) {
            script.onload = function () {
                _this.setAttributeValue(_util_constants__WEBPACK_IMPORTED_MODULE_4__["AppWidgetAttributes"].IsInit, "true");
            };
            document.body.appendChild(script);
        }
        else {
            this.setAttributeValue(_util_constants__WEBPACK_IMPORTED_MODULE_4__["AppWidgetAttributes"].IsInit, "true");
        }
    };
    MicroAppRendererComponent.prototype.subscribeToAppLaunch = function () {
        var _this = this;
        this.appLaunchReqSub = this.launchReqSvc.appLaunchSubject.subscribe(function () {
            _this.reload();
        });
    };
    MicroAppRendererComponent.prototype.setupElementIO = function () {
        this.attachInitCallbackListener();
        this.attachHttpRequestListener();
        this.attachHttpAbortListener();
        this.attachUserStateCallbackListener();
    };
    MicroAppRendererComponent.prototype.attachInitCallbackListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_4__["CustomEventListeners"].OnInitCallback, function ($event) {
            if (_this.isFunction($event.detail.callback)) {
                console.log("onInitCallback attached: " + _this.app.appTitle);
                _this.initCallback = $event.detail.callback;
                var userState = _util_cloner__WEBPACK_IMPORTED_MODULE_7__["Cloner"].cloneObject(_this.ptSvc.userState);
                var coreServices = _util_cloner__WEBPACK_IMPORTED_MODULE_7__["Cloner"].cloneObject(_this.ptSvc.coreServicesMap);
                var state = _this.launchReqSvc.appState || {};
                var baseUrl = "" + _util_constants__WEBPACK_IMPORTED_MODULE_4__["FileServerURL"] + _this.ptSvc.buildDirectoryPath + "/";
                _this.initCallback({
                    userState: userState,
                    coreServices: coreServices,
                    state: state,
                    baseUrl: baseUrl
                });
            }
        });
    };
    MicroAppRendererComponent.prototype.attachHttpRequestListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_4__["CustomEventListeners"].OnHttpRequestEvent, function ($event) {
            var request = $event.detail;
            request.appId = _this.app.docId;
            _this.httpControllerSvc.send(request, _this.app);
        });
    };
    MicroAppRendererComponent.prototype.attachHttpAbortListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_4__["CustomEventListeners"].OnHttpAbortEvent, function ($event) {
            _this.httpControllerSvc.cancelRequest($event.detail);
        });
    };
    MicroAppRendererComponent.prototype.attachUserStateCallbackListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_4__["CustomEventListeners"].OnUserStateCallback, function ($event) {
            if (_this.isFunction($event.detail.callback)) {
                console.log("onUserStateCallback attached: " + _this.app.appTitle);
                _this.userStateCallback = $event.detail.callback;
                _this.userStateCallback(_util_cloner__WEBPACK_IMPORTED_MODULE_7__["Cloner"].cloneObject(_this.ptSvc.userState));
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('app'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], MicroAppRendererComponent.prototype, "app", null);
    MicroAppRendererComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-micro-app-renderer',
            template: __webpack_require__(/*! ./micro-app-renderer.component.html */ "./src/app/app-renderers/micro-app-renderer/micro-app-renderer.component.html"),
            styles: [__webpack_require__(/*! ./micro-app-renderer.component.scss */ "./src/app/app-renderers/micro-app-renderer/micro-app-renderer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_project_tracker_service__WEBPACK_IMPORTED_MODULE_6__["ProjectTrackerService"],
            _services_app_launch_request_service__WEBPACK_IMPORTED_MODULE_5__["AppLaunchRequestService"],
            _services_http_request_controller_service__WEBPACK_IMPORTED_MODULE_3__["HttpRequestControllerService"]])
    ], MicroAppRendererComponent);
    return MicroAppRendererComponent;
}(_renderer__WEBPACK_IMPORTED_MODULE_2__["Renderer"]));



/***/ }),

/***/ "./src/app/app-renderers/renderer.ts":
/*!*******************************************!*\
  !*** ./src/app/app-renderers/renderer.ts ***!
  \*******************************************/
/*! exports provided: Renderer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Renderer", function() { return Renderer; });
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/index.js");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(uuid__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../util/constants */ "./src/app/util/constants.ts");


var Renderer = /** @class */ (function () {
    function Renderer() {
        this.containerId = uuid__WEBPACK_IMPORTED_MODULE_0__["v4"]();
        this.isInitialized = false;
    }
    Renderer.prototype.reload = function () {
        this.destroy();
        this.load();
    };
    Renderer.prototype.destroy = function () {
        if (this.customElem) {
            this.customElem.remove();
        }
    };
    Renderer.prototype.setAttributeValue = function (name, value) {
        if (this.customElem) {
            this.customElem.setAttribute(name, value);
        }
    };
    Renderer.prototype.buildScriptTag = function (buildDirectoryPath, bootstrap) {
        var scriptSrc = "" + _util_constants__WEBPACK_IMPORTED_MODULE_1__["FileServerURL"] + buildDirectoryPath + "/" + bootstrap;
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.className = "od-script";
        script.id = uuid__WEBPACK_IMPORTED_MODULE_0__["v4"]();
        script.src = scriptSrc;
        return script;
    };
    Renderer.prototype.buildCustomElement = function (tag) {
        var customEl = document.createElement(tag);
        customEl.id = uuid__WEBPACK_IMPORTED_MODULE_0__["v4"]();
        customEl.style.height = '100%';
        return customEl;
    };
    Renderer.prototype.isFunction = function (func) {
        return (typeof func === "function");
    };
    Renderer.prototype.makeCallback = function (func, params) {
        if (this.isFunction(func)) {
            func(params);
        }
    };
    Renderer.prototype.scriptExists = function (url) {
        var scripts = document.querySelectorAll('.od-script');
        var exists = false;
        for (var i = 0; i < scripts.length; ++i) {
            if (scripts[i].getAttribute("src") === url) {
                exists = true;
                break;
            }
        }
        if (exists) {
            return true;
        }
        return false;
    };
    return Renderer;
}());



/***/ }),

/***/ "./src/app/app-renderers/widget-renderer/widget-renderer.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/app-renderers/widget-renderer/widget-renderer.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"full-height\">\n    <div class=\"full-height\">\n        <div [class]=\"'card card-box-shadow margin-bottom ' + this.format.cardClass\" *ngIf=\"!minimized\">\n            <div class=\"widget-head\">\n                <div class=\"widget-title\">{{widget.widgetTitle}}</div>\n\n                <div class=\"widget-controls\">\n                    <button mat-icon-button [class]=\"format.leftBtn.class\" [disabled]=\"format.leftBtn.disabled\" (click)=\"leftBtnClick.emit(null)\">\n                        <mat-icon>{{format.leftBtn.icon}}</mat-icon>\n                    </button>\n\n                    <button mat-icon-button [class]=\"format.middleBtn.class\" [disabled]=\"format.middleBtn.disabled\" (click)=\"middleBtnClick.emit(null)\">\n                        <mat-icon>{{format.middleBtn.icon}}</mat-icon>\n                    </button>\n\n                    <button mat-icon-button [class]=\"format.rightBtn.class\" [disabled]=\"format.rightBtn.disabled\" (click)=\"rightBtnClick.emit(null)\">\n                        <mat-icon>{{format.rightBtn.icon}}</mat-icon>\n                    </button>\n\n                </div>\n            </div>\n\n            <div class=\"widget-float-body\">\n                <div [class]=\"'widget-float-body ' + this.format.widgetBodyClass\">\n                    <div class=\"full-height\" [id]=\"containerId\" (stateEvent)=\"temp($event)\"></div>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"widget-head\" *ngIf=\"minimized\">\n            <div class=\"widget-title pr-1\">{{widget.widgetTitle}}</div>\n\n            <div class=\"widget-controls\">\n\n                <button mat-icon-button [class]=\"format.leftBtn.class\" [disabled]=\"format.leftBtn.disabled\" (click)=\"leftBtnClick.emit(null)\">\n                    <mat-icon>{{format.leftBtn.icon}}</mat-icon>\n                </button>\n\n                <button mat-icon-button [class]=\"format.middleBtn.class\" [disabled]=\"format.middleBtn.disabled\" (click)=\"middleBtnClick.emit(null)\">\n                    <mat-icon>{{format.middleBtn.icon}}</mat-icon>\n                </button>\n\n                <button mat-icon-button [class]=\"format.rightBtn.class\" [disabled]=\"format.rightBtn.disabled\" (click)=\"rightBtnClick.emit(null)\">\n                    <mat-icon>{{format.rightBtn.icon}}</mat-icon>\n                </button>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/app-renderers/widget-renderer/widget-renderer.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/app-renderers/widget-renderer/widget-renderer.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC1yZW5kZXJlcnMvd2lkZ2V0LXJlbmRlcmVyL3dpZGdldC1yZW5kZXJlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app-renderers/widget-renderer/widget-renderer.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/app-renderers/widget-renderer/widget-renderer.component.ts ***!
  \****************************************************************************/
/*! exports provided: WidgetRendererComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetRendererComponent", function() { return WidgetRendererComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _renderer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../renderer */ "./src/app/app-renderers/renderer.ts");
/* harmony import */ var _models_widget_renderer_format_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models/widget-renderer-format.model */ "./src/app/models/widget-renderer-format.model.ts");
/* harmony import */ var _services_app_launch_request_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/app-launch-request.service */ "./src/app/services/app-launch-request.service.ts");
/* harmony import */ var _services_http_request_controller_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/http-request-controller.service */ "./src/app/services/http-request-controller.service.ts");
/* harmony import */ var _util_constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../util/constants */ "./src/app/util/constants.ts");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");
/* harmony import */ var _util_cloner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../util/cloner */ "./src/app/util/cloner.ts");











var WidgetRendererComponent = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](WidgetRendererComponent, _super);
    function WidgetRendererComponent(ptSvc, httpControllerSvc, appLaunchSvc) {
        var _this = _super.call(this) || this;
        _this.ptSvc = ptSvc;
        _this.httpControllerSvc = httpControllerSvc;
        _this.appLaunchSvc = appLaunchSvc;
        _this.minimized = false;
        _this.format = {
            cardClass: '', widgetBodyClass: "",
            leftBtn: { class: "", icon: "", disabled: true },
            middleBtn: { class: "", icon: "", disabled: true },
            rightBtn: { class: "", icon: "", disabled: true }
        };
        _this.leftBtnClick = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.middleBtnClick = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        _this.rightBtnClick = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        return _this;
    }
    Object.defineProperty(WidgetRendererComponent.prototype, "widget", {
        get: function () {
            return this._widget;
        },
        set: function (widget) {
            this._widget = widget;
            this.destroy();
            if (this.isInitialized) {
                this.load();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetRendererComponent.prototype, "format", {
        get: function () {
            return this._format;
        },
        set: function (format) {
            this._format = format;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetRendererComponent.prototype, "minimized", {
        get: function () {
            return this._minimized;
        },
        set: function (minimized) {
            this._minimized = minimized;
            if (this.widget && !this.minimized) {
                this.load();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetRendererComponent.prototype, "resize", {
        set: function (resize) {
            var _this = this;
            resize.subscribe(function (resize) {
                if (_this.customElem) {
                    _this.makeCallback(_this.resizeCallback, resize);
                }
            });
        },
        enumerable: true,
        configurable: true
    });
    WidgetRendererComponent.prototype.ngOnInit = function () {
        this.subscribeToUserSession();
        this.subscribeToReload();
        this.subscribeToReloadWidget();
    };
    WidgetRendererComponent.prototype.ngAfterViewInit = function () {
        this.isInitialized = true;
        if (this.widget && !this.minimized) {
            this.load();
        }
    };
    WidgetRendererComponent.prototype.ngOnDestroy = function () {
        this.destroy();
        if (this.userSessionSub) {
            this.userSessionSub.unsubscribe();
        }
        if (this.cacheSub) {
            this.cacheSub.unsubscribe();
        }
        if (this.reloadSub) {
            this.reloadSub.unsubscribe();
        }
        if (this.reloadWidgetSub) {
            this.reloadWidgetSub.unsubscribe();
        }
    };
    WidgetRendererComponent.prototype.load = function () {
        var _this = this;
        var container = document.getElementById(this.containerId);
        this.customElem = this.buildCustomElement(this.widget.widgetTag);
        this.setupElementIO();
        container.appendChild(this.customElem);
        var script = this.buildScriptTag(this.ptSvc.buildDirectoryPath, this.widget.widgetBootstrap);
        if (!this.scriptExists(script.src)) {
            script.onload = function () {
                _this.setAttributeValue(_util_constants__WEBPACK_IMPORTED_MODULE_7__["AppWidgetAttributes"].IsInit, "true");
            };
            document.body.appendChild(script);
        }
        else {
            this.setAttributeValue(_util_constants__WEBPACK_IMPORTED_MODULE_7__["AppWidgetAttributes"].IsInit, "true");
        }
    };
    WidgetRendererComponent.prototype.setupElementIO = function () {
        this.attachInitCallbackListener();
        this.attachHttpRequestListener();
        this.attachHttpAbortListener();
        this.attachAppLaunchRequestListener();
        this.attachWidgetStateChangeListener();
        this.attachUserStateCallbackListener();
        this.attachResizeCallbackListener();
        this.attachWidgetCacheCallbackListener();
        this.attachWidgetCacheWriteListener();
    };
    WidgetRendererComponent.prototype.attachInitCallbackListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_7__["CustomEventListeners"].OnInitCallback, function ($event) {
            if (_this.isFunction($event.detail.callback)) {
                console.log("onInitCallback attached: " + _this.widget.widgetTitle);
                _this.initCallback = $event.detail.callback;
                var userState = _util_cloner__WEBPACK_IMPORTED_MODULE_9__["Cloner"].cloneObject(_this.ptSvc.userState);
                var coreServices = _util_cloner__WEBPACK_IMPORTED_MODULE_9__["Cloner"].cloneObject(_this.ptSvc.coreServicesMap);
                var cache = _this.ptSvc.readFromCache(_this.widget.docId);
                var state = _this.ptSvc.widgetStates.get(_this.widget.docId) || {};
                var baseUrl = "" + _util_constants__WEBPACK_IMPORTED_MODULE_7__["FileServerURL"] + _this.ptSvc.buildDirectoryPath + "/";
                _this.initCallback({
                    userState: userState,
                    coreServices: coreServices,
                    cache: cache,
                    state: state,
                    baseUrl: baseUrl
                });
            }
        });
    };
    WidgetRendererComponent.prototype.attachHttpRequestListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_7__["CustomEventListeners"].OnHttpRequestEvent, function ($event) {
            var request = $event.detail;
            request.appId = _this.app.docId;
            request.widgetId = _this.widget.docId;
            _this.httpControllerSvc.send(request, _this.app);
        });
    };
    WidgetRendererComponent.prototype.attachHttpAbortListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_7__["CustomEventListeners"].OnHttpAbortEvent, function ($event) {
            _this.httpControllerSvc.cancelRequest($event.detail);
        });
    };
    WidgetRendererComponent.prototype.attachUserStateCallbackListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_7__["CustomEventListeners"].OnUserStateCallback, function ($event) {
            if (_this.isFunction($event.detail.callback)) {
                console.log("onUserStateCallback attached: " + _this.widget.widgetTitle);
                _this.userStateCallback = $event.detail.callback;
                _this.userStateCallback(_util_cloner__WEBPACK_IMPORTED_MODULE_9__["Cloner"].cloneObject(_this.ptSvc.userState));
            }
        });
    };
    WidgetRendererComponent.prototype.attachResizeCallbackListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_7__["CustomEventListeners"].OnResizeCallback, function ($event) {
            if (_this.isFunction($event.detail.callback)) {
                console.log("onResizeCallback attached: " + _this.widget.widgetTitle);
                _this.resizeCallback = $event.detail.callback;
            }
        });
    };
    WidgetRendererComponent.prototype.attachWidgetCacheCallbackListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_7__["CustomEventListeners"].OnWidgetCacheCallback, function ($event) {
            if (_this.isFunction($event.detail.callback)) {
                console.log("onWidgetCacheCallback attached: " + _this.widget.widgetTitle);
                _this.widgetCacheCallback = $event.detail.callback;
                _this.cacheSub = _this.ptSvc.subscribeToCache(_this.widget.docId).subscribe(function (value) {
                    _this.widgetCacheCallback(_util_cloner__WEBPACK_IMPORTED_MODULE_9__["Cloner"].cloneObject(value));
                });
            }
        });
    };
    WidgetRendererComponent.prototype.attachWidgetStateChangeListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_7__["CustomEventListeners"].OnStateChangeEvent, function ($event) {
            console.log("onStateChange event emitted: " + _this.widget.widgetTitle);
            console.log($event.detail);
            _this.ptSvc.widgetStates.set(_this.widget.docId, $event.detail);
        });
    };
    WidgetRendererComponent.prototype.attachAppLaunchRequestListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_7__["CustomEventListeners"].OnAppLaunchRequestEvent, function ($event) {
            console.log("onAppLaunchRequest event emitted: " + _this.widget.widgetTitle);
            console.log($event.detail);
            _this.appLaunchSvc.requestLaunch({
                launchPath: "",
                data: $event.detail
            });
        });
    };
    WidgetRendererComponent.prototype.attachWidgetCacheWriteListener = function () {
        var _this = this;
        this.customElem.addEventListener(_util_constants__WEBPACK_IMPORTED_MODULE_7__["CustomEventListeners"].OnSharedWidgetCacheWrite, function ($event) {
            console.log("onSharedWidgetCacheWrite event emitted: " + _this.widget.widgetTitle);
            console.log($event.detail);
            _this.ptSvc.writeToCache(_this.widget.docId, $event.detail);
        });
    };
    WidgetRendererComponent.prototype.subscribeToUserSession = function () {
        var _this = this;
        this.userSessionSub = this.ptSvc.userStateSub.subscribe(function (userState) {
            _this.makeCallback(_this.userStateCallback, _util_cloner__WEBPACK_IMPORTED_MODULE_9__["Cloner"].cloneObject(_this.ptSvc.userState));
        });
    };
    WidgetRendererComponent.prototype.subscribeToReload = function () {
        var _this = this;
        this.reloadSub = this.ptSvc.reloadSub.subscribe(function () {
            _this.reload();
        });
    };
    WidgetRendererComponent.prototype.subscribeToReloadWidget = function () {
        var _this = this;
        this.reloadWidgetSub = this.ptSvc.reloadWidgetSub.subscribe(function (widget) {
            if (widget.docId === _this.widget.docId) {
                _this.reload();
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], WidgetRendererComponent.prototype, "app", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('widget'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], WidgetRendererComponent.prototype, "widget", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('format'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_widget_renderer_format_model__WEBPACK_IMPORTED_MODULE_4__["WidgetRendererFormat"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_models_widget_renderer_format_model__WEBPACK_IMPORTED_MODULE_4__["WidgetRendererFormat"]])
    ], WidgetRendererComponent.prototype, "format", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('minimized'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], WidgetRendererComponent.prototype, "minimized", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('resize'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]])
    ], WidgetRendererComponent.prototype, "resize", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], WidgetRendererComponent.prototype, "leftBtnClick", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], WidgetRendererComponent.prototype, "middleBtnClick", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], WidgetRendererComponent.prototype, "rightBtnClick", void 0);
    WidgetRendererComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-widget-renderer',
            template: __webpack_require__(/*! ./widget-renderer.component.html */ "./src/app/app-renderers/widget-renderer/widget-renderer.component.html"),
            styles: [__webpack_require__(/*! ./widget-renderer.component.scss */ "./src/app/app-renderers/widget-renderer/widget-renderer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_project_tracker_service__WEBPACK_IMPORTED_MODULE_8__["ProjectTrackerService"],
            _services_http_request_controller_service__WEBPACK_IMPORTED_MODULE_6__["HttpRequestControllerService"],
            _services_app_launch_request_service__WEBPACK_IMPORTED_MODULE_5__["AppLaunchRequestService"]])
    ], WidgetRendererComponent);
    return WidgetRendererComponent;
}(_renderer__WEBPACK_IMPORTED_MODULE_3__["Renderer"]));



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _micro_app_builder_main_micro_app_builder_main_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./micro-app-builder-main/micro-app-builder-main.component */ "./src/app/micro-app-builder-main/micro-app-builder-main.component.ts");
/* harmony import */ var _micro_app_viewer_main_micro_app_viewer_main_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./micro-app-viewer-main/micro-app-viewer-main.component */ "./src/app/micro-app-viewer-main/micro-app-viewer-main.component.ts");





var routes = [
    {
        path: 'build',
        component: _micro_app_builder_main_micro_app_builder_main_component__WEBPACK_IMPORTED_MODULE_3__["MicroAppBuilderMainComponent"]
    },
    {
        path: 'test',
        component: _micro_app_viewer_main_micro_app_viewer_main_component__WEBPACK_IMPORTED_MODULE_4__["MicroAppViewerMainComponent"]
    },
    {
        path: '',
        redirectTo: 'build',
        pathMatch: 'full'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"height: 100vh;\">\n  <mat-toolbar class=\"bg-darkgray matToolbar\" *ngIf=\"showNavigation\">\n    <button mat-icon-button id=\"leftNavTrigger\" (click)=\"sidenavOpened = !sidenavOpened\" class=\"mr-1\" [ngClass]=\"{'opened': sidenavOpened}\" matTooltip=\"Toggle Menu\">\n      <span></span><span></span><span></span>\n    </button>\n    <span><app-sidebar-logo></app-sidebar-logo></span>\n  </mat-toolbar>\n  <mat-sidenav-container #main fxFlexFill>\n    <mat-sidenav #sidenav fxLayout=\"column\" [ngClass]=\"{'opened': sidenavOpened, 'collapsed': !sidenavOpened}\" id=\"sidebar\" mode=\"side\" fixedInViewport=\"true\" [(opened)]=\"sidenavOpened\" *ngIf=\"showNavigation\">\n      <div fxLayout=\"column\">\n        <mat-list>\n          <app-sidebar-menu></app-sidebar-menu>\n        </mat-list>\n      </div>\n    </mat-sidenav>\n    <mat-sidenav-content fxFlexFill id=\"main-content\" [ngClass]=\"{'margin-none': !showNavigation, 'minimize': showNavigation}\">\n      <router-outlet></router-outlet>\n    </mat-sidenav-content>\n  </mat-sidenav-container>\n</div>\n\n\n\n\n\n<!-- <button mat-icon-button class=\"opacity-half bg-primary zindex-9999 position-fixed\" id=\"leftNavTrigger\"\n  (click)=\"sidenavOpened = !sidenavOpened\" [ngClass]=\"{'opened': sidenavOpened}\" matTooltip=\"Toggle Menu\"\n  *ngIf=\"showNavigation\">\n  <span></span><span></span><span></span>\n</button>\n\n<mat-sidenav-container>\n\n  <mat-sidenav [ngClass]=\"{'opened': sidenavOpened, 'collapsed': !sidenavOpened}\" id=\"sidebar\" mode=\"side\"\n    fixedInViewport=\"true\" [(opened)]=\"sidenavOpened\" *ngIf=\"showNavigation\">\n    <mat-list>\n      <app-sidebar-logo></app-sidebar-logo>\n      <app-sidebar-menu></app-sidebar-menu>\n    </mat-list>\n  </mat-sidenav>\n\n  <mat-sidenav-content id=\"main-content\" [ngClass]=\"{'margin-none': !showNavigation, 'minimize': showNavigation}\">\n    <router-outlet></router-outlet>\n  </mat-sidenav-content>\n\n</mat-sidenav-container> -->"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".matToolbar {\n  position: sticky;\n  position: -webkit-sticky;\n  top: 0;\n  z-index: 1000;\n  height: 55px;\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2); }\n\n.spacer {\n  flex: 1 1 auto; }\n\n#leftNavTrigger {\n  background: transparent;\n  -webkit-appearance: none;\n     -moz-appearance: none;\n          appearance: none;\n  position: relative;\n  width: 35px;\n  height: 35px;\n  -webkit-transform: rotate(0deg);\n          transform: rotate(0deg);\n  transition: 0.4s ease-in-out;\n  margin-left: -2px; }\n\n#leftNavTrigger span {\n    display: block;\n    position: absolute;\n    height: 2px;\n    width: 15px;\n    background: #fff;\n    border-radius: 3px;\n    opacity: 1;\n    left: 10px;\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n    transition: 0.2s ease-in-out; }\n\n#leftNavTrigger span:nth-child(1) {\n      top: 13px;\n      -webkit-transform-origin: left center;\n              transform-origin: left center; }\n\n#leftNavTrigger span:nth-child(2) {\n      top: 17px;\n      -webkit-transform-origin: left center;\n              transform-origin: left center; }\n\n#leftNavTrigger span:nth-child(3) {\n      top: 21px;\n      -webkit-transform-origin: left center;\n              transform-origin: left center; }\n\n#leftNavTrigger.opened span:nth-child(1) {\n    -webkit-transform: rotate(45deg);\n            transform: rotate(45deg);\n    top: 12px;\n    left: 11px; }\n\n#leftNavTrigger.opened span:nth-child(2) {\n    width: 0;\n    opacity: 0; }\n\n#leftNavTrigger.opened span:nth-child(3) {\n    -webkit-transform: rotate(-45deg);\n            transform: rotate(-45deg);\n    top: 23px;\n    left: 11px; }\n\napp-sidebar-menu .mat-list-base {\n  margin-bottom: 90px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2phbWVzL0RvY3VtZW50cy9VS0kvUmVwb3NpdG9yaWVzL0RldmVsb3Blci9vZDM2MC1kZXZlbG9wZXIvc3JjL2FwcC9hcHAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsd0JBQXdCO0VBQ3hCLE1BQU07RUFDTixhQUFhO0VBQ2IsWUFBWTtFQUNaLHdDQUErQixFQUFBOztBQUdqQztFQUNFLGNBQWMsRUFBQTs7QUFJaEI7RUFDRSx1QkFBdUI7RUFDdkIsd0JBQWdCO0tBQWhCLHFCQUFnQjtVQUFoQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxZQUFZO0VBQ1osK0JBQXVCO1VBQXZCLHVCQUF1QjtFQUN2Qiw0QkFBNEI7RUFDNUIsaUJBQWlCLEVBQUE7O0FBUm5CO0lBV0ksY0FBYztJQUNkLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsV0FBVztJQUNYLGdCQUFnQjtJQUNoQixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFVBQVU7SUFDViwrQkFBdUI7WUFBdkIsdUJBQXVCO0lBQ3ZCLDRCQUE0QixFQUFBOztBQXBCaEM7TUF1Qk0sU0FBUztNQUNULHFDQUE2QjtjQUE3Qiw2QkFBNkIsRUFBQTs7QUF4Qm5DO01BNEJNLFNBQVM7TUFDVCxxQ0FBNkI7Y0FBN0IsNkJBQTZCLEVBQUE7O0FBN0JuQztNQWlDTSxTQUFTO01BQ1QscUNBQTZCO2NBQTdCLDZCQUE2QixFQUFBOztBQWxDbkM7SUF1Q0ksZ0NBQXdCO1lBQXhCLHdCQUF3QjtJQUN4QixTQUFTO0lBQ1QsVUFBVSxFQUFBOztBQXpDZDtJQTZDSSxRQUFRO0lBQ1IsVUFBVSxFQUFBOztBQTlDZDtJQWtESSxpQ0FBeUI7WUFBekIseUJBQXlCO0lBQ3pCLFNBQVM7SUFDVCxVQUFVLEVBQUE7O0FBS2Q7RUFDRSxtQkFBbUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vID09PT09PT09PT09PT09PT09PSBQT1JUQUwgLyBNQUlOID09PT09PT09PT09PT09PT09PT0gLy9cblxuLm1hdFRvb2xiYXIge1xuICBwb3NpdGlvbjogc3RpY2t5O1xuICBwb3NpdGlvbjogLXdlYmtpdC1zdGlja3k7XG4gIHRvcDogMDtcbiAgei1pbmRleDogMTAwMDtcbiAgaGVpZ2h0OiA1NXB4O1xuICBib3gtc2hhZG93OiAwIDVweCA1cHggcmdiYSgjMDAwLCAwLjIpO1xufVxuXG4uc3BhY2VyIHtcbiAgZmxleDogMSAxIGF1dG87XG59XG5cbi8vID09PSBMRUZUIE5BViA9PT09PT09PT09PT09PT09PT09IC8vXG4jbGVmdE5hdlRyaWdnZXIge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgYXBwZWFyYW5jZTogbm9uZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB3aWR0aDogMzVweDtcbiAgaGVpZ2h0OiAzNXB4O1xuICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcbiAgdHJhbnNpdGlvbjogMC40cyBlYXNlLWluLW91dDtcbiAgbWFyZ2luLWxlZnQ6IC0ycHg7XG5cbiAgc3BhbiB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMnB4O1xuICAgIHdpZHRoOiAxNXB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIG9wYWNpdHk6IDE7XG4gICAgbGVmdDogMTBweDtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcbiAgICB0cmFuc2l0aW9uOiAwLjJzIGVhc2UtaW4tb3V0O1xuXG4gICAgJjpudGgtY2hpbGQoMSkge1xuICAgICAgdG9wOiAxM3B4O1xuICAgICAgdHJhbnNmb3JtLW9yaWdpbjogbGVmdCBjZW50ZXI7XG4gICAgfVxuXG4gICAgJjpudGgtY2hpbGQoMikge1xuICAgICAgdG9wOiAxN3B4O1xuICAgICAgdHJhbnNmb3JtLW9yaWdpbjogbGVmdCBjZW50ZXI7XG4gICAgfVxuXG4gICAgJjpudGgtY2hpbGQoMykge1xuICAgICAgdG9wOiAyMXB4O1xuICAgICAgdHJhbnNmb3JtLW9yaWdpbjogbGVmdCBjZW50ZXI7XG4gICAgfVxuICB9XG5cbiAgJi5vcGVuZWQgc3BhbjpudGgtY2hpbGQoMSkge1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcbiAgICB0b3A6IDEycHg7XG4gICAgbGVmdDogMTFweDtcbiAgfVxuXG4gICYub3BlbmVkIHNwYW46bnRoLWNoaWxkKDIpIHtcbiAgICB3aWR0aDogMDtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG5cbiAgJi5vcGVuZWQgc3BhbjpudGgtY2hpbGQoMykge1xuICAgIHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7XG4gICAgdG9wOiAyM3B4O1xuICAgIGxlZnQ6IDExcHg7XG4gIH1cbn1cblxuLy8gQ3JlYXRlIHNwYWNlIGZvciB3aWRnZXQgaG90YmFyIGFuZCB1c2VyIG1lbnUgdG8gb3ZlcmxhcCBuYXZcbmFwcC1zaWRlYmFyLW1lbnUgLm1hdC1saXN0LWJhc2Uge1xuICBtYXJnaW4tYm90dG9tOiA5MHB4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_http_request_monitor_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/http-request-monitor.service */ "./src/app/services/http-request-monitor.service.ts");



var AppComponent = /** @class */ (function () {
    function AppComponent(httpMonitorSvc) {
        this.httpMonitorSvc = httpMonitorSvc;
        this.showNavigation = true;
        this.sidenavOpened = true;
    }
    AppComponent.prototype.ngOnInit = function () {
        //this.httpMonitorSvc.start();
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_http_request_monitor_service__WEBPACK_IMPORTED_MODULE_2__["HttpRequestMonitorService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/esm5/sidenav.es5.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/esm5/list.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/esm5/tooltip.es5.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/esm5/button.es5.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/esm5/card.es5.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/esm5/toolbar.es5.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/esm5/menu.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/esm5/select.es5.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/esm5/chips.es5.js");
/* harmony import */ var angular2_draggable__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! angular2-draggable */ "./node_modules/angular2-draggable/fesm5/angular2-draggable.js");
/* harmony import */ var ngx_highlight_js__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ngx-highlight-js */ "./node_modules/ngx-highlight-js/fesm5/ngx-highlight-js.js");
/* harmony import */ var _app_renderers_app_renderers_module__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./app-renderers/app-renderers.module */ "./src/app/app-renderers/app-renderers.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _micro_app_viewer_main_micro_app_viewer_main_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./micro-app-viewer-main/micro-app-viewer-main.component */ "./src/app/micro-app-viewer-main/micro-app-viewer-main.component.ts");
/* harmony import */ var _sidebar_logo_sidebar_logo_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./sidebar-logo/sidebar-logo.component */ "./src/app/sidebar-logo/sidebar-logo.component.ts");
/* harmony import */ var _sidebar_menu_sidebar_menu_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./sidebar-menu/sidebar-menu.component */ "./src/app/sidebar-menu/sidebar-menu.component.ts");
/* harmony import */ var _micro_app_toolbar_micro_app_toolbar_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./micro-app-toolbar/micro-app-toolbar.component */ "./src/app/micro-app-toolbar/micro-app-toolbar.component.ts");
/* harmony import */ var _micro_app_builder_main_micro_app_builder_main_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./micro-app-builder-main/micro-app-builder-main.component */ "./src/app/micro-app-builder-main/micro-app-builder-main.component.ts");
/* harmony import */ var _micro_app_directory_picker_micro_app_directory_picker_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./micro-app-directory-picker/micro-app-directory-picker.component */ "./src/app/micro-app-directory-picker/micro-app-directory-picker.component.ts");
/* harmony import */ var _app_info_builder_app_info_builder_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./app-info-builder/app-info-builder.component */ "./src/app/app-info-builder/app-info-builder.component.ts");
/* harmony import */ var _micro_app_info_builder_micro_app_info_builder_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./micro-app-info-builder/micro-app-info-builder.component */ "./src/app/micro-app-info-builder/micro-app-info-builder.component.ts");
/* harmony import */ var _widget_builder_widget_builder_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./widget-builder/widget-builder.component */ "./src/app/widget-builder/widget-builder.component.ts");
/* harmony import */ var _api_call_builder_api_call_builder_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./api-call-builder/api-call-builder.component */ "./src/app/api-call-builder/api-call-builder.component.ts");
/* harmony import */ var _micro_app_builder_finalize_micro_app_builder_finalize_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./micro-app-builder-finalize/micro-app-builder-finalize.component */ "./src/app/micro-app-builder-finalize/micro-app-builder-finalize.component.ts");
/* harmony import */ var _widget_windows_widget_windows_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./widget-windows/widget-windows.component */ "./src/app/widget-windows/widget-windows.component.ts");
/* harmony import */ var _user_state_editor_user_state_editor_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./user-state-editor/user-state-editor.component */ "./src/app/user-state-editor/user-state-editor.component.ts");
/* harmony import */ var _core_services_editor_core_services_editor_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./core-services-editor/core-services-editor.component */ "./src/app/core-services-editor/core-services-editor.component.ts");
/* harmony import */ var _project_builder_project_builder_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./project-builder/project-builder.component */ "./src/app/project-builder/project-builder.component.ts");
/* harmony import */ var _project_picker_project_picker_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./project-picker/project-picker.component */ "./src/app/project-picker/project-picker.component.ts");
/* harmony import */ var _ui_directory_picker_ui_directory_picker_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./ui-directory-picker/ui-directory-picker.component */ "./src/app/ui-directory-picker/ui-directory-picker.component.ts");
/* harmony import */ var _build_file_linker_build_file_linker_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./build-file-linker/build-file-linker.component */ "./src/app/build-file-linker/build-file-linker.component.ts");
















































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_29__["AppComponent"],
                _micro_app_viewer_main_micro_app_viewer_main_component__WEBPACK_IMPORTED_MODULE_30__["MicroAppViewerMainComponent"],
                _sidebar_logo_sidebar_logo_component__WEBPACK_IMPORTED_MODULE_31__["SidebarLogoComponent"],
                _sidebar_menu_sidebar_menu_component__WEBPACK_IMPORTED_MODULE_32__["SidebarMenuComponent"],
                _micro_app_toolbar_micro_app_toolbar_component__WEBPACK_IMPORTED_MODULE_33__["MicroAppToolbarComponent"],
                _micro_app_builder_main_micro_app_builder_main_component__WEBPACK_IMPORTED_MODULE_34__["MicroAppBuilderMainComponent"],
                _micro_app_directory_picker_micro_app_directory_picker_component__WEBPACK_IMPORTED_MODULE_35__["MicroAppDirectoryPickerComponent"],
                _app_info_builder_app_info_builder_component__WEBPACK_IMPORTED_MODULE_36__["AppInfoBuilderComponent"],
                _micro_app_info_builder_micro_app_info_builder_component__WEBPACK_IMPORTED_MODULE_37__["MicroAppInfoBuilderComponent"],
                _widget_builder_widget_builder_component__WEBPACK_IMPORTED_MODULE_38__["WidgetBuilderComponent"],
                _api_call_builder_api_call_builder_component__WEBPACK_IMPORTED_MODULE_39__["ApiCallBuilderComponent"],
                _micro_app_builder_finalize_micro_app_builder_finalize_component__WEBPACK_IMPORTED_MODULE_40__["MicroAppBuilderFinalizeComponent"],
                _widget_windows_widget_windows_component__WEBPACK_IMPORTED_MODULE_41__["WidgetWindowsComponent"],
                _user_state_editor_user_state_editor_component__WEBPACK_IMPORTED_MODULE_42__["UserStateEditorComponent"],
                _core_services_editor_core_services_editor_component__WEBPACK_IMPORTED_MODULE_43__["CoreServicesEditorComponent"],
                _project_builder_project_builder_component__WEBPACK_IMPORTED_MODULE_44__["ProjectBuilderComponent"],
                _project_picker_project_picker_component__WEBPACK_IMPORTED_MODULE_45__["ProjectPickerComponent"],
                _ui_directory_picker_ui_directory_picker_component__WEBPACK_IMPORTED_MODULE_46__["UiDirectoryPickerComponent"],
                _build_file_linker_build_file_linker_component__WEBPACK_IMPORTED_MODULE_47__["BuildFileLinkerComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_7__["MatSidenavModule"],
                _angular_material_list__WEBPACK_IMPORTED_MODULE_8__["MatListModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_9__["MatGridListModule"],
                _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_10__["MatTooltipModule"],
                _angular_material_button__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"],
                _angular_material_icon__WEBPACK_IMPORTED_MODULE_13__["MatIconModule"],
                _angular_material_card__WEBPACK_IMPORTED_MODULE_12__["MatCardModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_14__["MatTabsModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_15__["MatTableModule"],
                _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_16__["MatToolbarModule"],
                _angular_material_menu__WEBPACK_IMPORTED_MODULE_17__["MatMenuModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__["MatCheckboxModule"],
                _angular_material_stepper__WEBPACK_IMPORTED_MODULE_19__["MatStepperModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_20__["MatInputModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_21__["MatFormFieldModule"],
                _angular_material_select__WEBPACK_IMPORTED_MODULE_22__["MatSelectModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_23__["MatExpansionModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_24__["MatDialogModule"],
                _angular_material_chips__WEBPACK_IMPORTED_MODULE_25__["MatChipsModule"],
                angular2_draggable__WEBPACK_IMPORTED_MODULE_26__["AngularDraggableModule"],
                ngx_highlight_js__WEBPACK_IMPORTED_MODULE_27__["HighlightJsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _app_renderers_app_renderers_module__WEBPACK_IMPORTED_MODULE_28__["AppRenderersModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_29__["AppComponent"]],
            entryComponents: [
                _user_state_editor_user_state_editor_component__WEBPACK_IMPORTED_MODULE_42__["UserStateEditorComponent"],
                _core_services_editor_core_services_editor_component__WEBPACK_IMPORTED_MODULE_43__["CoreServicesEditorComponent"],
                _project_builder_project_builder_component__WEBPACK_IMPORTED_MODULE_44__["ProjectBuilderComponent"],
                _project_picker_project_picker_component__WEBPACK_IMPORTED_MODULE_45__["ProjectPickerComponent"],
                _ui_directory_picker_ui_directory_picker_component__WEBPACK_IMPORTED_MODULE_46__["UiDirectoryPickerComponent"],
                _build_file_linker_build_file_linker_component__WEBPACK_IMPORTED_MODULE_47__["BuildFileLinkerComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/build-file-linker/build-file-linker.component.html":
/*!********************************************************************!*\
  !*** ./src/app/build-file-linker/build-file-linker.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-list>\n  <mat-list-item>\n    <small>\n      <strong>Build Path: </strong>{{systemFilePath}}\n    </small>\n  </mat-list-item>\n  <mat-list-item style=\"padding-top: 40px;\">\n    <a [href]=\"buildFilePath\" target=\"_blank\" download mat-raised-button class=\"bg-green color-white\">Download</a>\n    <button mat-raised-button class=\"bg-red color-white\" (click)=\"cancelClicked()\">Cancel</button>\n  </mat-list-item>\n</mat-list>"

/***/ }),

/***/ "./src/app/build-file-linker/build-file-linker.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/build-file-linker/build-file-linker.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2J1aWxkLWZpbGUtbGlua2VyL2J1aWxkLWZpbGUtbGlua2VyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/build-file-linker/build-file-linker.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/build-file-linker/build-file-linker.component.ts ***!
  \******************************************************************/
/*! exports provided: BuildFileLinkerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuildFileLinkerComponent", function() { return BuildFileLinkerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../util/constants */ "./src/app/util/constants.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var BuildFileLinkerComponent = /** @class */ (function () {
    function BuildFileLinkerComponent() {
        this.cancelled = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    Object.defineProperty(BuildFileLinkerComponent.prototype, "buildFilePath", {
        get: function () {
            return this._buildFilePath;
        },
        set: function (path) {
            this._buildFilePath = _util_constants__WEBPACK_IMPORTED_MODULE_2__["FileServerURL"] + path;
            this.systemFilePath = path;
        },
        enumerable: true,
        configurable: true
    });
    BuildFileLinkerComponent.prototype.ngOnInit = function () {
    };
    BuildFileLinkerComponent.prototype.cancelClicked = function () {
        this.cancelled.next();
    };
    BuildFileLinkerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-build-file-linker',
            template: __webpack_require__(/*! ./build-file-linker.component.html */ "./src/app/build-file-linker/build-file-linker.component.html"),
            styles: [__webpack_require__(/*! ./build-file-linker.component.scss */ "./src/app/build-file-linker/build-file-linker.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BuildFileLinkerComponent);
    return BuildFileLinkerComponent;
}());



/***/ }),

/***/ "./src/app/core-services-editor/core-services-editor.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/core-services-editor/core-services-editor.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"position:relative;margin-top:-20px;\">\n  <h2>Core Services Editor</h2>\n  <button mat-icon-button class=\"position-absolute opacity-half\" style=\"top:-5px;right:0;\" (click)=\"onCancel()\">\n    <mat-icon>clear</mat-icon>\n  </button>\n  <div mat-dialog-content>\n    <textarea #coreServicesEditor spellcheck=\"false\" style=\"width: 100%; box-sizing: border-box; height: 400px; resize: none\">\n    {{data.coreServicesMap | json}}\n    </textarea>\n    <div mat-dialog-footer>\n        <span class=\"color-red\" *ngIf=\"jsonError\">Invalid JSON</span>\n      </div>\n  </div>\n  <div mat-dialog-actions>\n    <button mat-raised-button  color=\"primary\" (click)=\"onSave()\" cdkFocusInitial matTooltip=\"Save Core Services\">Save</button>\n    <button mat-button (click)=\"onCancel()\">Cancel</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/core-services-editor/core-services-editor.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/core-services-editor/core-services-editor.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUtc2VydmljZXMtZWRpdG9yL2NvcmUtc2VydmljZXMtZWRpdG9yLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/core-services-editor/core-services-editor.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/core-services-editor/core-services-editor.component.ts ***!
  \************************************************************************/
/*! exports provided: CoreServicesEditorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreServicesEditorComponent", function() { return CoreServicesEditorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");



var CoreServicesEditorComponent = /** @class */ (function () {
    function CoreServicesEditorComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.jsonError = false;
    }
    CoreServicesEditorComponent.prototype.ngOnInit = function () {
    };
    CoreServicesEditorComponent.prototype.onSave = function () {
        try {
            var newCoreServicesMap = JSON.parse(this.coreServicesEditor.nativeElement.value);
            this.dialogRef.close(Object.assign({}, newCoreServicesMap));
        }
        catch (err) {
            this.jsonError = true;
        }
    };
    CoreServicesEditorComponent.prototype.onCancel = function () {
        this.dialogRef.close(null);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('coreServicesEditor'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], CoreServicesEditorComponent.prototype, "coreServicesEditor", void 0);
    CoreServicesEditorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-core-services-editor',
            template: __webpack_require__(/*! ./core-services-editor.component.html */ "./src/app/core-services-editor/core-services-editor.component.html"),
            styles: [__webpack_require__(/*! ./core-services-editor.component.scss */ "./src/app/core-services-editor/core-services-editor.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object])
    ], CoreServicesEditorComponent);
    return CoreServicesEditorComponent;
}());



/***/ }),

/***/ "./src/app/micro-app-builder-finalize/micro-app-builder-finalize.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/micro-app-builder-finalize/micro-app-builder-finalize.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>Finalize Application</h2>\n<mat-grid-list [cols]=\"2\">\n  <mat-grid-tile [colspan]=\"1\">\n    <textarea highlight-js [options]=\"{}\" [lang]=\"'json'\">\n      {{manifest | json}}\n    </textarea>\n  </mat-grid-tile>\n\n  <mat-grid-tile [colspan]=\"1\">\n    <app-project-builder (buildComplete)=\"buildComplete()\" (onToggleShadowDomCheck)='onToggleShadowDomCheck($event)'></app-project-builder>\n  </mat-grid-tile>\n\n</mat-grid-list>"

/***/ }),

/***/ "./src/app/micro-app-builder-finalize/micro-app-builder-finalize.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/micro-app-builder-finalize/micro-app-builder-finalize.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21pY3JvLWFwcC1idWlsZGVyLWZpbmFsaXplL21pY3JvLWFwcC1idWlsZGVyLWZpbmFsaXplLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/micro-app-builder-finalize/micro-app-builder-finalize.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/micro-app-builder-finalize/micro-app-builder-finalize.component.ts ***!
  \************************************************************************************/
/*! exports provided: MicroAppBuilderFinalizeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MicroAppBuilderFinalizeComponent", function() { return MicroAppBuilderFinalizeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");




var MicroAppBuilderFinalizeComponent = /** @class */ (function () {
    function MicroAppBuilderFinalizeComponent(router, ptSvc) {
        this.router = router;
        this.ptSvc = ptSvc;
        this.enableShadowDomCheck = true;
    }
    MicroAppBuilderFinalizeComponent.prototype.ngOnInit = function () {
        this.subscribeToManifestUpdates();
    };
    MicroAppBuilderFinalizeComponent.prototype.ngOnDestroy = function () {
        this.manifestSub.unsubscribe();
    };
    MicroAppBuilderFinalizeComponent.prototype.buildComplete = function () {
        this.router.navigateByUrl("/test?enableShadowDomCheck=" + this.enableShadowDomCheck);
    };
    MicroAppBuilderFinalizeComponent.prototype.onToggleShadowDomCheck = function (event) {
        this.enableShadowDomCheck = event;
    };
    MicroAppBuilderFinalizeComponent.prototype.subscribeToManifestUpdates = function () {
        var _this = this;
        this.manifestSub = this.ptSvc.manifestSub.subscribe(function (m) {
            setTimeout(function () {
                _this.manifest = m;
            });
        });
    };
    MicroAppBuilderFinalizeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-micro-app-builder-finalize',
            template: __webpack_require__(/*! ./micro-app-builder-finalize.component.html */ "./src/app/micro-app-builder-finalize/micro-app-builder-finalize.component.html"),
            styles: [__webpack_require__(/*! ./micro-app-builder-finalize.component.scss */ "./src/app/micro-app-builder-finalize/micro-app-builder-finalize.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_3__["ProjectTrackerService"]])
    ], MicroAppBuilderFinalizeComponent);
    return MicroAppBuilderFinalizeComponent;
}());



/***/ }),

/***/ "./src/app/micro-app-builder-main/micro-app-builder-main.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/micro-app-builder-main/micro-app-builder-main.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Build Project</h1>\n<mat-horizontal-stepper [linear]=\"true\" #stepper>\n  <mat-step [completed]=\"stepOneCompleted\">\n    <ng-template matStepLabel>Project Directory</ng-template>\n    <app-micro-app-directory-picker [directoryPath]=\"directoryPath\" (directoryListed)=\"directoryListed($event)\"></app-micro-app-directory-picker>\n  </mat-step>\n\n  <mat-step [completed]=\"stepTwoCompleted\">\n    <ng-template matStepLabel>Application</ng-template>\n    <app-app-info-builder (submitted)=\"appInfoProvided()\"></app-app-info-builder>\n  </mat-step>\n\n  <mat-step [completed]=\"stepThreeCompleted\">\n    <ng-template matStepLabel>MicroApp</ng-template>\n    <app-micro-app-info-builder (submitted)=\"microAppInfoProvided()\"></app-micro-app-info-builder>\n  </mat-step>\n\n  <mat-step [completed]=\"stepFourCompleted\">\n    <ng-template matStepLabel>Widgets</ng-template>\n    <app-widget-builder (submitted)=\"widgetInfoProvided()\"></app-widget-builder>\n  </mat-step>\n\n  <mat-step [completed]=\"stepFiveCompleted\">\n    <ng-template matStepLabel>API Calls</ng-template>\n    <app-api-call-builder (submitted)=\"apiCallInfoProvided()\"></app-api-call-builder>\n  </mat-step>\n\n  <mat-step [completed]=\"stepSixCompleted\">\n    <ng-template matStepLabel>Finalize</ng-template>\n    <app-micro-app-builder-finalize></app-micro-app-builder-finalize>\n  </mat-step>\n</mat-horizontal-stepper>"

/***/ }),

/***/ "./src/app/micro-app-builder-main/micro-app-builder-main.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/micro-app-builder-main/micro-app-builder-main.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21pY3JvLWFwcC1idWlsZGVyLW1haW4vbWljcm8tYXBwLWJ1aWxkZXItbWFpbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/micro-app-builder-main/micro-app-builder-main.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/micro-app-builder-main/micro-app-builder-main.component.ts ***!
  \****************************************************************************/
/*! exports provided: MicroAppBuilderMainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MicroAppBuilderMainComponent", function() { return MicroAppBuilderMainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/esm5/stepper.es5.js");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");
/* harmony import */ var _services_socket_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/socket.service */ "./src/app/services/socket.service.ts");





var MicroAppBuilderMainComponent = /** @class */ (function () {
    function MicroAppBuilderMainComponent(cdRef, ptSvc, socketSvc) {
        this.cdRef = cdRef;
        this.ptSvc = ptSvc;
        this.socketSvc = socketSvc;
        this.stepOneCompleted = false;
        this.stepTwoCompleted = false;
        this.stepThreeCompleted = false;
        this.stepFourCompleted = false;
        this.stepFiveCompleted = false;
        this.stepSixCompleted = false;
    }
    MicroAppBuilderMainComponent.prototype.ngOnInit = function () {
        this.subscribeToSlotReadManifest();
        this.subscribeToNewProject();
        this.subscribeToProjectOpened();
        this.directoryPath = this.ptSvc.directoryPath;
    };
    MicroAppBuilderMainComponent.prototype.ngOnDestroy = function () {
        this.slotReadManifestSub.unsubscribe();
        this.newProjectSub.unsubscribe();
        this.projectOpenedSub.unsubscribe();
    };
    MicroAppBuilderMainComponent.prototype.directoryListed = function ($event) {
        this.resetAll();
        if ($event.directoryPath !== this.ptSvc.directoryPath) {
            this.ptSvc.clearProject();
        }
        this.ptSvc.directoryPath = $event.directoryPath;
        this.ptSvc.files = $event.files;
        if (this.ptSvc.files.length) {
            if (this.ptSvc.files.includes("od-manifest.json")) {
                this.socketSvc.signalReadManifest(this.ptSvc.directoryPath);
            }
            this.stepOneCompleted = true;
            this.cdRef.detectChanges();
            this.stepper.next();
        }
    };
    MicroAppBuilderMainComponent.prototype.subscribeToSlotReadManifest = function () {
        var _this = this;
        this.slotReadManifestSub = this.socketSvc.slotReadManifestSub.subscribe(function (manifest) {
            if (manifest) {
                _this.ptSvc.manifest = manifest;
            }
        });
    };
    MicroAppBuilderMainComponent.prototype.subscribeToNewProject = function () {
        var _this = this;
        this.newProjectSub = this.ptSvc.newProjectSub.subscribe(function () {
            _this.ptSvc.clearProject();
            _this.resetAll();
        });
    };
    MicroAppBuilderMainComponent.prototype.subscribeToProjectOpened = function () {
        var _this = this;
        this.projectOpenedSub = this.ptSvc.projectOpenedSub.subscribe(function () {
            _this.resetAll();
            _this.directoryPath = _this.ptSvc.directoryPath;
        });
    };
    MicroAppBuilderMainComponent.prototype.resetAll = function () {
        this.stepOneCompleted = false;
        this.stepTwoCompleted = false;
        this.stepThreeCompleted = false;
        this.stepFourCompleted = false;
        this.stepFiveCompleted = false;
        this.stepSixCompleted = false;
        this.stepper.reset();
        this.cdRef.detectChanges();
    };
    MicroAppBuilderMainComponent.prototype.appInfoProvided = function () {
        this.stepTwoCompleted = true;
        this.cdRef.detectChanges();
        this.stepper.next();
    };
    MicroAppBuilderMainComponent.prototype.microAppInfoProvided = function () {
        this.stepThreeCompleted = true;
        this.cdRef.detectChanges();
        this.stepper.next();
    };
    MicroAppBuilderMainComponent.prototype.widgetInfoProvided = function () {
        this.stepFourCompleted = true;
        this.cdRef.detectChanges();
        this.stepper.next();
    };
    MicroAppBuilderMainComponent.prototype.apiCallInfoProvided = function () {
        this.stepFiveCompleted = true;
        this.cdRef.detectChanges();
        this.stepper.next();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('stepper'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_stepper__WEBPACK_IMPORTED_MODULE_2__["MatStepper"])
    ], MicroAppBuilderMainComponent.prototype, "stepper", void 0);
    MicroAppBuilderMainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-micro-app-builder-main',
            template: __webpack_require__(/*! ./micro-app-builder-main.component.html */ "./src/app/micro-app-builder-main/micro-app-builder-main.component.html"),
            styles: [__webpack_require__(/*! ./micro-app-builder-main.component.scss */ "./src/app/micro-app-builder-main/micro-app-builder-main.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_3__["ProjectTrackerService"],
            _services_socket_service__WEBPACK_IMPORTED_MODULE_4__["SocketService"]])
    ], MicroAppBuilderMainComponent);
    return MicroAppBuilderMainComponent;
}());



/***/ }),

/***/ "./src/app/micro-app-directory-picker/micro-app-directory-picker.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/micro-app-directory-picker/micro-app-directory-picker.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>Project Directory</h2>\n<mat-form-field>\n  <input matInput placeholder=\"Local Path\" [(ngModel)]=\"directoryPath\" required>\n  <mat-error *ngIf=\"showError\">\n    Unable to list files. Please check your directory path.\n  </mat-error>\n</mat-form-field>\n<button mat-mini-fab color=\"primary\" (click)=\"showDirectoryPicker()\"><mat-icon>publish</mat-icon></button>\n<div class=\"mt-3\">\n  <button mat-raised-button color=\"primary\" (click)=\"listFiles()\">Next</button>\n</div>\n"

/***/ }),

/***/ "./src/app/micro-app-directory-picker/micro-app-directory-picker.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/micro-app-directory-picker/micro-app-directory-picker.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21pY3JvLWFwcC1kaXJlY3RvcnktcGlja2VyL21pY3JvLWFwcC1kaXJlY3RvcnktcGlja2VyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/micro-app-directory-picker/micro-app-directory-picker.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/micro-app-directory-picker/micro-app-directory-picker.component.ts ***!
  \************************************************************************************/
/*! exports provided: MicroAppDirectoryPickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MicroAppDirectoryPickerComponent", function() { return MicroAppDirectoryPickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_socket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/socket.service */ "./src/app/services/socket.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _ui_directory_picker_ui_directory_picker_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../ui-directory-picker/ui-directory-picker.component */ "./src/app/ui-directory-picker/ui-directory-picker.component.ts");





var MicroAppDirectoryPickerComponent = /** @class */ (function () {
    function MicroAppDirectoryPickerComponent(socketSvc, dialogSvc) {
        this.socketSvc = socketSvc;
        this.dialogSvc = dialogSvc;
        this.showError = false;
        this.directoryListed = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    MicroAppDirectoryPickerComponent.prototype.ngOnInit = function () {
        this.subscribeToSlotListFiles();
    };
    MicroAppDirectoryPickerComponent.prototype.ngOnDestroy = function () {
        this.slotListFilesSub.unsubscribe();
    };
    MicroAppDirectoryPickerComponent.prototype.listFiles = function () {
        if (this.directoryPath.trim().length) {
            this.socketSvc.signalListFiles(this.directoryPath);
        }
    };
    MicroAppDirectoryPickerComponent.prototype.showDirectoryPicker = function () {
        var _this = this;
        var dialogRef = this.dialogSvc.open(_ui_directory_picker_ui_directory_picker_component__WEBPACK_IMPORTED_MODULE_4__["UiDirectoryPickerComponent"], {
            width: '800px'
        });
        dialogRef.afterOpened().subscribe(function () {
            dialogRef.componentInstance.directorySelectedSub.subscribe(function (directory) {
                _this.directoryPath = directory;
                dialogRef.close();
            });
            dialogRef.componentInstance.closedSub.subscribe(function () {
                dialogRef.close();
            });
        });
    };
    MicroAppDirectoryPickerComponent.prototype.subscribeToSlotListFiles = function () {
        var _this = this;
        this.slotListFilesSub = this.socketSvc.slotListFilesSub.subscribe(function (files) {
            _this.directoryListed.emit({ directoryPath: _this.directoryPath, files: files });
            _this.showError = !files.length;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], MicroAppDirectoryPickerComponent.prototype, "directoryPath", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppDirectoryPickerComponent.prototype, "directoryListed", void 0);
    MicroAppDirectoryPickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-micro-app-directory-picker',
            template: __webpack_require__(/*! ./micro-app-directory-picker.component.html */ "./src/app/micro-app-directory-picker/micro-app-directory-picker.component.html"),
            styles: [__webpack_require__(/*! ./micro-app-directory-picker.component.scss */ "./src/app/micro-app-directory-picker/micro-app-directory-picker.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_socket_service__WEBPACK_IMPORTED_MODULE_2__["SocketService"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]])
    ], MicroAppDirectoryPickerComponent);
    return MicroAppDirectoryPickerComponent;
}());



/***/ }),

/***/ "./src/app/micro-app-info-builder/micro-app-info-builder.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/micro-app-info-builder/micro-app-info-builder.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h2>Configure MicroApp</h2>\n<mat-list>\n  <mat-list-item class=\"mb-4\">\n    <mat-form-field>\n      <input matInput placeholder=\"Custom Element Tag\" [ngModel]=\"manifest.app.tag\" (keyup)=\"appTagChanged($event)\">\n    </mat-form-field>\n  </mat-list-item>\n\n  <mat-list-item class=\"mb-4\">\n    <mat-form-field>\n      <mat-label>Bootstrap Module</mat-label>\n      <mat-select [(value)]=\"manifest.app.bootstrapModule\">\n        <mat-option *ngFor=\"let file of jsFiles\" [value]=\"file\">\n          {{file}}\n        </mat-option>\n      </mat-select>\n    </mat-form-field>\n  </mat-list-item>\n\n  <mat-list-item>\n    <button mat-button matStepperPrevious class=\"mr-1\">Back</button>\n    <button mat-raised-button color=\"primary\" (click)=\"submit()\">Next</button>\n  </mat-list-item>\n</mat-list>"

/***/ }),

/***/ "./src/app/micro-app-info-builder/micro-app-info-builder.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/micro-app-info-builder/micro-app-info-builder.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21pY3JvLWFwcC1pbmZvLWJ1aWxkZXIvbWljcm8tYXBwLWluZm8tYnVpbGRlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/micro-app-info-builder/micro-app-info-builder.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/micro-app-info-builder/micro-app-info-builder.component.ts ***!
  \****************************************************************************/
/*! exports provided: MicroAppInfoBuilderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MicroAppInfoBuilderComponent", function() { return MicroAppInfoBuilderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util_file_mapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../util/file-mapper */ "./src/app/util/file-mapper.ts");
/* harmony import */ var _util_text_transform__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../util/text-transform */ "./src/app/util/text-transform.ts");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");





var MicroAppInfoBuilderComponent = /** @class */ (function () {
    function MicroAppInfoBuilderComponent(ptSvc) {
        this.ptSvc = ptSvc;
        this.imageFiles = new Array();
        this.jsFiles = new Array();
        this.submitted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    MicroAppInfoBuilderComponent.prototype.ngOnInit = function () {
        this.subscribeToFileUpdates();
        this.subscribeToManifestUpdates();
    };
    MicroAppInfoBuilderComponent.prototype.ngOnDestroy = function () {
        this.filesSub.unsubscribe();
        this.manifestSub.unsubscribe();
    };
    MicroAppInfoBuilderComponent.prototype.appTagChanged = function ($event) {
        var value = $event.currentTarget.value;
        if (value.length) {
            this.manifest.app.tag = _util_text_transform__WEBPACK_IMPORTED_MODULE_3__["TextTransform"].transformCustomElementTag(value);
        }
        else {
            this.manifest.app.tag = null;
        }
    };
    MicroAppInfoBuilderComponent.prototype.submit = function () {
        this.submitted.emit();
    };
    MicroAppInfoBuilderComponent.prototype.subscribeToFileUpdates = function () {
        var _this = this;
        this.filesSub = this.ptSvc.filesSub.subscribe(function (files) {
            _this.imageFiles = _util_file_mapper__WEBPACK_IMPORTED_MODULE_2__["FileMapper"].mapImageFiles(files);
            _this.jsFiles = _util_file_mapper__WEBPACK_IMPORTED_MODULE_2__["FileMapper"].mapJSFiles(files);
        });
    };
    MicroAppInfoBuilderComponent.prototype.subscribeToManifestUpdates = function () {
        var _this = this;
        this.manifestSub = this.ptSvc.manifestSub.subscribe(function (manifest) {
            _this.manifest = manifest;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppInfoBuilderComponent.prototype, "submitted", void 0);
    MicroAppInfoBuilderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-micro-app-info-builder',
            template: __webpack_require__(/*! ./micro-app-info-builder.component.html */ "./src/app/micro-app-info-builder/micro-app-info-builder.component.html"),
            styles: [__webpack_require__(/*! ./micro-app-info-builder.component.scss */ "./src/app/micro-app-info-builder/micro-app-info-builder.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_project_tracker_service__WEBPACK_IMPORTED_MODULE_4__["ProjectTrackerService"]])
    ], MicroAppInfoBuilderComponent);
    return MicroAppInfoBuilderComponent;
}());



/***/ }),

/***/ "./src/app/micro-app-toolbar/micro-app-toolbar.component.html":
/*!********************************************************************!*\
  !*** ./src/app/micro-app-toolbar/micro-app-toolbar.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 class=\"mr-2 display-inline-block float-left\">{{title}}</h1>\n<div class=\"mt-2\">\n    <button mat-mini-fab color=\"secondary\" [matMenuTriggerFor]=\"devMenu\" class=\"mr-1\">\n        <mat-icon>more_vert</mat-icon>\n    </button>\n    <mat-menu #devMenu=\"matMenu\">\n        <button mat-menu-item (click)=\"userStateClicked()\">\n            <mat-icon>person</mat-icon>\n            <span>Edit User State</span>\n        </button>\n        <button mat-menu-item (click)=\"coreServicesClicked()\">\n            <mat-icon>device_hub</mat-icon>\n            <span>Edit Core Services</span>\n        </button>\n        <button mat-menu-item [matMenuTriggerFor]=\"widgetMenu\">\n            <mat-icon>widgets</mat-icon>\n            <span>View Widgets</span>\n        </button>\n        <button mat-menu-item [matMenuTriggerFor]=\"reloadMenu\">\n            <mat-icon>refresh</mat-icon>\n            <span>Refresh Instances</span>\n        </button>\n        <button mat-menu-item [matMenuTriggerFor]=\"clearStateMenu\">\n            <mat-icon>clear_all</mat-icon>\n            <span>Clear States</span>\n        </button>\n    </mat-menu>\n\n    <button mat-mini-fab class=\"mr-1\" color=\"primary\" (click)=\"rebuildClicked()\" matTooltip=\"Reuild Project\">\n        <mat-icon>build</mat-icon>\n    </button>\n    <button mat-mini-fab class=\"bg-green\" (click)=\"publishClicked()\" matTooltip=\"Publish Project\">\n        <mat-icon>publish</mat-icon>\n    </button>\n\n    <mat-menu #widgetMenu=\"matMenu\">\n        <button mat-menu-item *ngFor=\"let widget of widgets\" (click)=\"widgetClicked(widget)\">\n            {{widget.widgetTitle}}\n        </button>\n    </mat-menu>\n\n</div>\n\n<mat-menu #reloadMenu=\"matMenu\">\n    <button mat-menu-item (click)=\"reloadClicked()\">\n        Refresh All\n    </button>\n    <button *ngIf=\"hasMicroApp\" mat-menu-item (click)=\"reloadAppClicked()\">\n        Refresh MicroApp\n    </button>\n    <button *ngFor=\"let widget of widgets\" mat-menu-item (click)=\"reloadWidgetClicked(widget)\">\n        Refresh {{widget.widgetTitle}}\n    </button>\n</mat-menu>\n\n<mat-menu #clearStateMenu=\"matMenu\">\n    <button mat-menu-item (click)=\"clearAllStatesClicked()\">\n        Clear All States\n    </button>\n    <button *ngIf=\"hasMicroApp\" mat-menu-item (click)=\"clearAppStateClicked()\">\n        Clear MicroApp State\n    </button>\n    <button *ngFor=\"let widget of widgets\" mat-menu-item (click)=\"clearWidgetStateClicked(widget)\">\n        Clear {{widget.widgetTitle}} State\n    </button>\n</mat-menu>\n\n\n\n\n\n\n\n\n\n\n\n<!-- <mat-grid-list cols=\"1\" rowHeight=\"65px\" class=\"mb-1\">\n        <mat-grid-tile>\n            <h1 class=\"display-inline-block\">\n                {{title}}\n            </h1>\n            <span>\n                <button mat-icon-button [matMenuTriggerFor]=\"menu\" class=\"mr-1\">\n                    <mat-icon>expand_more</mat-icon>\n                </button>\n                <mat-menu #menu=\"matMenu\">\n                    <button mat-icon-button (click)=\"userStateClicked()\">\n                            <mat-icon>person</mat-icon>\n                            <span>View User State</span>\n                        </button>\n                        <button mat-icon-button (click)=\"coreServicesClicked()\">\n                            <mat-icon>device_hub</mat-icon>\n                            <span>View Core Services</span>\n                        </button>\n                        <button mat-icon-button [matMenuTriggerFor]=\"widgetMenu\">\n                            <mat-icon>widgets</mat-icon>\n                            <span>View Widgets</span>\n                        </button>\n                        <button mat-icon-button [matMenuTriggerFor]=\"reloadMenu\">\n                            <mat-icon>refresh</mat-icon>\n                            <span>Refresh States</span>\n                        </button>\n                        <button mat-icon-button [matMenuTriggerFor]=\"clearStateMenu\">\n                            <mat-icon>clear_all</mat-icon>\n                            <span>Clear States</span>\n                        </button>\n                </mat-menu>\n                <button mat-mini-fab color=\"secondary\" (click)=\"rebuildClicked()\">\n                    <mat-icon>build</mat-icon>\n                </button>\n                <button mat-mini-fab color=\"primary\" (click)=\"publishClicked()\">\n                    <mat-icon>publish</mat-icon>\n                </button>\n            </span>\n        </mat-grid-tile>\n    </mat-grid-list>\n    \n    <mat-menu #widgetMenu=\"matMenu\">\n        <button mat-list-item *ngFor=\"let widget of widgets\" (click)=\"widgetClicked(widget)\">\n            {{widget.widgetTitle}}\n        </button>\n    </mat-menu>\n    \n    <mat-menu #reloadMenu=\"matMenu\">\n        <button mat-list-item (click)=\"reloadClicked()\">\n            Refresh All\n        </button>\n        <button *ngIf=\"hasMicroApp\" mat-list-item (click)=\"reloadAppClicked()\">\n            Refresh MicroApp\n        </button>\n        <button *ngFor=\"let widget of widgets\" mat-list-item (click)=\"reloadWidgetClicked(widget)\">\n            Refresh {{widget.widgetTitle}}\n        </button>\n    </mat-menu>\n    \n    <mat-menu #clearStateMenu=\"matMenu\">\n        <button mat-list-item (click)=\"clearAllStatesClicked()\">\n            Clear All States\n        </button>\n        <button *ngIf=\"hasMicroApp\" mat-list-item (click)=\"clearAppStateClicked()\">\n            Clear MicroApp State\n        </button>\n        <button *ngFor=\"let widget of widgets\" mat-list-item (click)=\"clearWidgetStateClicked(widget)\">\n            Clear {{widget.widgetTitle}} State\n        </button>\n    </mat-menu> -->"

/***/ }),

/***/ "./src/app/micro-app-toolbar/micro-app-toolbar.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/micro-app-toolbar/micro-app-toolbar.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-toolbar h1 {\n  font-size: 32px;\n  font-weight: 700; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2phbWVzL0RvY3VtZW50cy9VS0kvUmVwb3NpdG9yaWVzL0RldmVsb3Blci9vZDM2MC1kZXZlbG9wZXIvc3JjL2FwcC9taWNyby1hcHAtdG9vbGJhci9taWNyby1hcHAtdG9vbGJhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQWU7RUFDZixnQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL21pY3JvLWFwcC10b29sYmFyL21pY3JvLWFwcC10b29sYmFyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWF0LXRvb2xiYXIgaDEge1xuICAgIGZvbnQtc2l6ZTogMzJweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/micro-app-toolbar/micro-app-toolbar.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/micro-app-toolbar/micro-app-toolbar.component.ts ***!
  \******************************************************************/
/*! exports provided: MicroAppToolbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MicroAppToolbarComponent", function() { return MicroAppToolbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MicroAppToolbarComponent = /** @class */ (function () {
    function MicroAppToolbarComponent() {
        this.launchWidget = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.editUserState = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.editCoreServices = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.reloadApp = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.reloadWidget = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.reload = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.rebuild = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.clearAllStates = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.clearAppState = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.clearWidgetState = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.publish = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.hasMicroApp = false;
    }
    Object.defineProperty(MicroAppToolbarComponent.prototype, "hasMicroApp", {
        get: function () {
            return this._hasMicroApp;
        },
        set: function (val) {
            this._hasMicroApp = val;
        },
        enumerable: true,
        configurable: true
    });
    MicroAppToolbarComponent.prototype.ngOnInit = function () {
    };
    MicroAppToolbarComponent.prototype.widgetClicked = function (widget) {
        this.launchWidget.emit(widget);
    };
    MicroAppToolbarComponent.prototype.userStateClicked = function () {
        this.editUserState.emit();
    };
    MicroAppToolbarComponent.prototype.coreServicesClicked = function () {
        this.editCoreServices.emit();
    };
    MicroAppToolbarComponent.prototype.reloadClicked = function () {
        this.reload.emit();
    };
    MicroAppToolbarComponent.prototype.reloadAppClicked = function () {
        this.reloadApp.emit();
    };
    MicroAppToolbarComponent.prototype.reloadWidgetClicked = function (widget) {
        this.reloadWidget.emit(widget);
    };
    MicroAppToolbarComponent.prototype.rebuildClicked = function () {
        this.rebuild.emit();
    };
    MicroAppToolbarComponent.prototype.clearAllStatesClicked = function () {
        this.clearAllStates.emit();
    };
    MicroAppToolbarComponent.prototype.clearAppStateClicked = function () {
        this.clearAppState.emit();
    };
    MicroAppToolbarComponent.prototype.clearWidgetStateClicked = function (widget) {
        this.clearWidgetState.emit(widget);
    };
    MicroAppToolbarComponent.prototype.publishClicked = function () {
        this.publish.emit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], MicroAppToolbarComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], MicroAppToolbarComponent.prototype, "widgets", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('hasMicroApp'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], MicroAppToolbarComponent.prototype, "hasMicroApp", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "launchWidget", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "editUserState", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "editCoreServices", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "reload", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "reloadApp", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "reloadWidget", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "rebuild", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "clearAllStates", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "clearAppState", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "clearWidgetState", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], MicroAppToolbarComponent.prototype, "publish", void 0);
    MicroAppToolbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-micro-app-toolbar',
            template: __webpack_require__(/*! ./micro-app-toolbar.component.html */ "./src/app/micro-app-toolbar/micro-app-toolbar.component.html"),
            styles: [__webpack_require__(/*! ./micro-app-toolbar.component.scss */ "./src/app/micro-app-toolbar/micro-app-toolbar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MicroAppToolbarComponent);
    return MicroAppToolbarComponent;
}());



/***/ }),

/***/ "./src/app/micro-app-viewer-main/micro-app-viewer-main.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/micro-app-viewer-main/micro-app-viewer-main.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-micro-app-toolbar \n[title]=\"appTitle\"\n[widgets]=\"widgets\"\n[hasMicroApp]=\"hasApp\"\n(launchWidget)=\"launchWidget($event)\"\n(editUserState)=\"editUserState()\"\n(editCoreServices)=\"editCoreServicesMap()\"\n(reload)=\"reload()\"\n(reloadApp)=\"reloadApp()\"\n(reloadWidget)=\"reloadWidget($event)\"\n(rebuild)=\"rebuildProject()\"\n(clearAllStates)=\"clearAllStates()\"\n(clearAppState)=\"clearAppState()\"\n(clearWidgetState)=\"clearWidgetState($event)\"\n(publish)=\"publish()\">\n</app-micro-app-toolbar>\n\n<app-micro-app-renderer *ngIf=\"app\" [app]=\"app\"></app-micro-app-renderer>\n\n<app-widget-windows></app-widget-windows>"

/***/ }),

/***/ "./src/app/micro-app-viewer-main/micro-app-viewer-main.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/micro-app-viewer-main/micro-app-viewer-main.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21pY3JvLWFwcC12aWV3ZXItbWFpbi9taWNyby1hcHAtdmlld2VyLW1haW4uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/micro-app-viewer-main/micro-app-viewer-main.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/micro-app-viewer-main/micro-app-viewer-main.component.ts ***!
  \**************************************************************************/
/*! exports provided: MicroAppViewerMainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MicroAppViewerMainComponent", function() { return MicroAppViewerMainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/index.js");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(uuid__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _services_widget_windows_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/widget-windows.service */ "./src/app/services/widget-windows.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _user_state_editor_user_state_editor_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../user-state-editor/user-state-editor.component */ "./src/app/user-state-editor/user-state-editor.component.ts");
/* harmony import */ var _core_services_editor_core_services_editor_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../core-services-editor/core-services-editor.component */ "./src/app/core-services-editor/core-services-editor.component.ts");
/* harmony import */ var _project_builder_project_builder_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../project-builder/project-builder.component */ "./src/app/project-builder/project-builder.component.ts");
/* harmony import */ var _services_app_launch_request_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/app-launch-request.service */ "./src/app/services/app-launch-request.service.ts");
/* harmony import */ var _services_socket_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/socket.service */ "./src/app/services/socket.service.ts");
/* harmony import */ var _build_file_linker_build_file_linker_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../build-file-linker/build-file-linker.component */ "./src/app/build-file-linker/build-file-linker.component.ts");













var MicroAppViewerMainComponent = /** @class */ (function () {
    function MicroAppViewerMainComponent(ptSvc, launchReqSvc, router, wwSvc, socketSvc, dialogSvc) {
        this.ptSvc = ptSvc;
        this.launchReqSvc = launchReqSvc;
        this.router = router;
        this.wwSvc = wwSvc;
        this.socketSvc = socketSvc;
        this.dialogSvc = dialogSvc;
        this.widgets = new Array();
        this.hasApp = false;
        this.enableShadowDomCheck = true;
    }
    MicroAppViewerMainComponent.prototype.ngOnInit = function () {
        if (this.ptSvc.buildManifest) {
            this.buildModels();
        }
        else {
            this.router.navigateByUrl('/build');
        }
        this.subscribeToZipProject();
        var queryParams = new URLSearchParams(window.location.search);
        if (queryParams.has('enableShadowDomCheck')) {
            this.enableShadowDomCheck = queryParams.get('enableShadowDomCheck') === 'true';
        }
    };
    MicroAppViewerMainComponent.prototype.ngOnDestroy = function () {
        if (this.zipProjectSub) {
            this.zipProjectSub.unsubscribe();
        }
    };
    MicroAppViewerMainComponent.prototype.launchWidget = function (widget) {
        this.wwSvc.addWindow({ app: this.app, widget: widget });
    };
    MicroAppViewerMainComponent.prototype.editUserState = function () {
        var _this = this;
        var dialogRef = this.dialogSvc.open(_user_state_editor_user_state_editor_component__WEBPACK_IMPORTED_MODULE_7__["UserStateEditorComponent"], {
            width: '90%',
            data: { userState: Object.assign({}, this.ptSvc.userState) }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                _this.ptSvc.userState = result;
            }
        });
    };
    MicroAppViewerMainComponent.prototype.editCoreServicesMap = function () {
        var _this = this;
        var dialogRef = this.dialogSvc.open(_core_services_editor_core_services_editor_component__WEBPACK_IMPORTED_MODULE_8__["CoreServicesEditorComponent"], {
            width: '90%',
            data: { coreServicesMap: Object.assign({}, this.ptSvc.coreServicesMap) }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                _this.ptSvc.coreServicesMap = result;
            }
        });
    };
    MicroAppViewerMainComponent.prototype.rebuildProject = function () {
        var _this = this;
        var dialogRef = this.dialogSvc.open(_project_builder_project_builder_component__WEBPACK_IMPORTED_MODULE_9__["ProjectBuilderComponent"], {
            width: '500px',
            disableClose: true
        });
        dialogRef.componentInstance.enableShadowDomCheck = this.enableShadowDomCheck;
        dialogRef.afterOpened().subscribe(function () {
            dialogRef.componentInstance.showCancelBtn = true;
            dialogRef.componentInstance.onToggleShadowDomCheck.subscribe(function (enabled) {
                _this.enableShadowDomCheck = enabled;
            });
            dialogRef.componentInstance.buildComplete.subscribe(function () {
                _this.buildModels();
                dialogRef.close();
            });
            dialogRef.componentInstance.cancelBuildSub.subscribe(function () {
                dialogRef.close();
            });
        });
    };
    MicroAppViewerMainComponent.prototype.reload = function () {
        this.ptSvc.reloadSub.next();
    };
    MicroAppViewerMainComponent.prototype.reloadApp = function () {
        this.ptSvc.reloadAppSub.next();
    };
    MicroAppViewerMainComponent.prototype.reloadWidget = function (widget) {
        this.ptSvc.reloadWidgetSub.next(widget);
    };
    MicroAppViewerMainComponent.prototype.clearAllStates = function () {
        this.launchReqSvc.appState = null;
        this.ptSvc.widgetStates.clear();
        this.ptSvc.reloadSub.next();
    };
    MicroAppViewerMainComponent.prototype.clearAppState = function () {
        this.launchReqSvc.appState = null;
        this.ptSvc.reloadAppSub.next();
    };
    MicroAppViewerMainComponent.prototype.clearWidgetState = function (widget) {
        this.ptSvc.widgetStates.delete(widget.docId);
        this.ptSvc.reloadWidgetSub.next(widget);
    };
    MicroAppViewerMainComponent.prototype.publish = function () {
        this.socketSvc.signalZipProject(this.ptSvc.buildDirectoryPath);
    };
    MicroAppViewerMainComponent.prototype.subscribeToZipProject = function () {
        var _this = this;
        this.zipProjectSub = this.socketSvc.slotZipProjectSub.subscribe(function (buildFilePath) {
            if (buildFilePath) {
                var dialogRef_1 = _this.dialogSvc.open(_build_file_linker_build_file_linker_component__WEBPACK_IMPORTED_MODULE_12__["BuildFileLinkerComponent"], {
                    width: '500px'
                });
                dialogRef_1.afterOpened().subscribe(function () {
                    dialogRef_1.componentInstance.buildFilePath = buildFilePath;
                    dialogRef_1.componentInstance.cancelled.subscribe(function () {
                        dialogRef_1.close();
                    });
                });
            }
        });
    };
    MicroAppViewerMainComponent.prototype.buildModels = function () {
        this.app = null;
        this.widgets = new Array();
        this.wwSvc.removeAll();
        this.appTitle = this.ptSvc.buildManifest.title;
        this.createAppModel();
        this.createWidgetModels();
    };
    MicroAppViewerMainComponent.prototype.createAppModel = function () {
        var manifest = this.ptSvc.buildManifest;
        if (manifest.app) {
            this.app = {
                docId: uuid__WEBPACK_IMPORTED_MODULE_4__["v4"](),
                appTag: manifest.app.tag,
                appBootstrap: manifest.app.bootstrapModule,
                appTitle: manifest.title,
                appIcon: manifest.iconImage,
                enabled: true,
                native: false,
                clientId: null,
                clientName: null,
                permissions: manifest.permissions,
                apiCalls: manifest.apiCalls
            };
            this.hasApp = true;
        }
        else {
            this.hasApp = false;
        }
    };
    MicroAppViewerMainComponent.prototype.createWidgetModels = function () {
        var _this = this;
        this.widgets = new Array();
        var manifest = this.ptSvc.buildManifest;
        manifest.widgets.forEach(function (mw) {
            _this.widgets.push({
                docId: uuid__WEBPACK_IMPORTED_MODULE_4__["v4"](),
                widgetTitle: mw.title,
                widgetTag: mw.tag,
                widgetBootstrap: mw.bootstrapModule,
                icon: mw.iconImage
            });
        });
    };
    MicroAppViewerMainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-micro-app-viewer-main',
            template: __webpack_require__(/*! ./micro-app-viewer-main.component.html */ "./src/app/micro-app-viewer-main/micro-app-viewer-main.component.html"),
            styles: [__webpack_require__(/*! ./micro-app-viewer-main.component.scss */ "./src/app/micro-app-viewer-main/micro-app-viewer-main.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_project_tracker_service__WEBPACK_IMPORTED_MODULE_2__["ProjectTrackerService"],
            _services_app_launch_request_service__WEBPACK_IMPORTED_MODULE_10__["AppLaunchRequestService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_widget_windows_service__WEBPACK_IMPORTED_MODULE_5__["WidgetWindowsService"],
            _services_socket_service__WEBPACK_IMPORTED_MODULE_11__["SocketService"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"]])
    ], MicroAppViewerMainComponent);
    return MicroAppViewerMainComponent;
}());



/***/ }),

/***/ "./src/app/models/widget-renderer-format.model.ts":
/*!********************************************************!*\
  !*** ./src/app/models/widget-renderer-format.model.ts ***!
  \********************************************************/
/*! exports provided: WidgetRendererFormat, WidgetRendererBtnFormat */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetRendererFormat", function() { return WidgetRendererFormat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetRendererBtnFormat", function() { return WidgetRendererBtnFormat; });
var WidgetRendererFormat = /** @class */ (function () {
    function WidgetRendererFormat() {
    }
    return WidgetRendererFormat;
}());

var WidgetRendererBtnFormat = /** @class */ (function () {
    function WidgetRendererBtnFormat() {
    }
    return WidgetRendererBtnFormat;
}());



/***/ }),

/***/ "./src/app/project-builder/project-builder.component.html":
/*!****************************************************************!*\
  !*** ./src/app/project-builder/project-builder.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-list>\n  <mat-divider></mat-divider>\n  <mat-list-item>\n    <mat-icon matListIcon *ngIf=\"manifestStatus === 1\" class=\"opacity-half\">query_builder</mat-icon>\n    <mat-icon matListIcon *ngIf=\"manifestStatus === 2\" class=\"color-green\">check_circle</mat-icon>\n    <mat-icon matListIcon *ngIf=\"manifestStatus === 3\" class=\"color-red\">error</mat-icon>\n    <h3 matLine>Validate Manifest</h3>\n  </mat-list-item>\n  <mat-divider></mat-divider>\n  <mat-list-item>\n    <mat-icon matListIcon *ngIf=\"copyStatus === 1\" class=\"opacity-half\">query_builder</mat-icon>\n    <mat-icon matListIcon *ngIf=\"copyStatus === 2\" class=\"color-green\">check_circle</mat-icon>\n    <mat-icon matListIcon *ngIf=\"copyStatus === 3\" class=\"color-red\">error</mat-icon>\n    <h3 matLine>Create Directory</h3>\n  </mat-list-item>\n  <mat-divider></mat-divider>\n  <mat-list-item>\n    <mat-icon matListIcon *ngIf=\"this.enableCodeObfuscation && obfuscationStatus === 1\" class=\"opacity-half\">query_builder</mat-icon>\n    <mat-icon matListIcon *ngIf=\"this.enableCodeObfuscation && obfuscationStatus === 2\" class=\"color-green\">check_circle</mat-icon>\n    <mat-icon matListIcon *ngIf=\"this.enableCodeObfuscation && obfuscationStatus === 3\" class=\"color-red\">error</mat-icon>\n    <mat-icon matListIcon *ngIf='!this.enableCodeObfuscation' class='color-gray'>remove_circle</mat-icon>\n    <h3 matLine [ngStyle]=\"{'opacity': this.enableCodeObfuscation ? '1' : '0.3'}\">Obfuscate Code</h3>\n    <!-- <mat-checkbox matTooltip='Toggle Code obfuscationStatus' [checked]='this.enableCodeObfuscation' (change)='this.toggleCodeObfuscation()' (click)='killEvent($event)'></mat-checkbox> -->\n  </mat-list-item>\n  <mat-divider></mat-divider>\n  <mat-list-item class='toggleable-item' (click)='this.toggleShadowDomCheck()'>\n    <mat-icon matListIcon *ngIf=\"this.enableShadowDomCheck && shadowDomStatus === 1\" class=\"opacity-half\">query_builder</mat-icon>\n    <mat-icon matListIcon *ngIf=\"this.enableShadowDomCheck && shadowDomStatus === 2\" class=\"color-green\">check_circle</mat-icon>\n    <mat-icon matListIcon *ngIf=\"this.enableShadowDomCheck && shadowDomStatus === 3\" class=\"color-red\">error</mat-icon>\n    <mat-icon matListIcon *ngIf='!this.enableShadowDomCheck' class='color-gray'>remove_circle</mat-icon>\n    <h3 matLine [ngStyle]=\"{'opacity': this.enableShadowDomCheck ? '1' : '0.3'}\">Validate Shadow DOM</h3>\n    <mat-checkbox matTooltip='Toggle Shadow DOM Validation' [checked]='this.enableShadowDomCheck' (change)='this.toggleShadowDomCheck($event)' (click)='killEvent($event)'></mat-checkbox>\n  </mat-list-item>\n  <mat-divider></mat-divider>\n  <mat-list-item style=\"padding-top: 40px;\">\n    <button mat-raised-button class=\"bg-green color-white\" (click)=\"validate()\">Build Project</button>\n    <button *ngIf=\"showCancelBtn\" mat-button (click)=\"cancelClicked()\">Cancel</button>\n  </mat-list-item>\n  <mat-list-item *ngFor=\"let error of errors\" class=\"color-red\">\n    {{error}}\n  </mat-list-item>\n</mat-list>"

/***/ }),

/***/ "./src/app/project-builder/project-builder.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/project-builder/project-builder.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  width: 100%; }\n\n.toggleable-item {\n  cursor: pointer; }\n\n::ng-deep .mat-checkbox-checked.mat-accent .mat-checkbox-ripple .mat-ripple-element {\n  opacity: 0.03 !important;\n  background-color: #2F74C7 !important; }\n\n::ng-deep .mat-checkbox-checked.mat-accent .mat-checkbox-background, .mat-checkbox-indeterminate.mat-accent .mat-checkbox-background {\n  background-color: #2F74C7; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2phbWVzL0RvY3VtZW50cy9VS0kvUmVwb3NpdG9yaWVzL0RldmVsb3Blci9vZDM2MC1kZXZlbG9wZXIvc3JjL2FwcC9wcm9qZWN0LWJ1aWxkZXIvcHJvamVjdC1idWlsZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBVyxFQUFBOztBQUdmO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUNJLHdCQUF3QjtFQUN4QixvQ0FBb0MsRUFBQTs7QUFHeEM7RUFDSSx5QkFBeUIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3Byb2plY3QtYnVpbGRlci9wcm9qZWN0LWJ1aWxkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLnRvZ2dsZWFibGUtaXRlbXtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbjo6bmctZGVlcCAubWF0LWNoZWNrYm94LWNoZWNrZWQubWF0LWFjY2VudCAubWF0LWNoZWNrYm94LXJpcHBsZSAubWF0LXJpcHBsZS1lbGVtZW50IHtcbiAgICBvcGFjaXR5OiAwLjAzICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzJGNzRDNyAhaW1wb3J0YW50O1xufVxuXG46Om5nLWRlZXAgLm1hdC1jaGVja2JveC1jaGVja2VkLm1hdC1hY2NlbnQgLm1hdC1jaGVja2JveC1iYWNrZ3JvdW5kLC5tYXQtY2hlY2tib3gtaW5kZXRlcm1pbmF0ZS5tYXQtYWNjZW50IC5tYXQtY2hlY2tib3gtYmFja2dyb3VuZCB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzJGNzRDNztcbn0iXX0= */"

/***/ }),

/***/ "./src/app/project-builder/project-builder.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/project-builder/project-builder.component.ts ***!
  \**************************************************************/
/*! exports provided: ProjectBuilderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectBuilderComponent", function() { return ProjectBuilderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_socket_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/socket.service */ "./src/app/services/socket.service.ts");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");





var Status;
(function (Status) {
    Status[Status["Pending"] = 1] = "Pending";
    Status[Status["Complete"] = 2] = "Complete";
    Status[Status["Error"] = 3] = "Error";
    Status[Status["Skipped"] = 4] = "Skipped";
})(Status || (Status = {}));
var ProjectBuilderComponent = /** @class */ (function () {
    function ProjectBuilderComponent(ptSvc, socketSvc) {
        this.ptSvc = ptSvc;
        this.socketSvc = socketSvc;
        this.buildComplete = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.cancelBuildSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.showCancelBtn = false;
        this.enableShadowDomCheck = true;
        this.enableCodeObfuscation = true;
        this.onToggleCodeObfuscation = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.onToggleShadowDomCheck = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    Object.defineProperty(ProjectBuilderComponent.prototype, "showCancelBtn", {
        get: function () {
            return this._showCancelBtn;
        },
        set: function (show) {
            this._showCancelBtn = show;
        },
        enumerable: true,
        configurable: true
    });
    ProjectBuilderComponent.prototype.ngOnInit = function () {
        this.subscribeToManifestUpdates();
        this.subscribeToSlotCopyProject();
        this.subscribeToSlotObfuscate();
        this.subscribeToSlotShadowDomCheck();
    };
    ProjectBuilderComponent.prototype.ngOnDestroy = function () {
        this.manifestSub.unsubscribe();
        this.slotCopyProjectSub.unsubscribe();
        this.slotObfuscateSub.unsubscribe();
        this.slotShadowDomCheckSub.unsubscribe();
    };
    ProjectBuilderComponent.prototype.cancelClicked = function () {
        this.cancelBuildSub.next();
    };
    ProjectBuilderComponent.prototype.toggleCodeObfuscation = function () {
        this.enableCodeObfuscation = !this.enableCodeObfuscation;
        this.onToggleCodeObfuscation.emit(this.enableCodeObfuscation);
    };
    ProjectBuilderComponent.prototype.toggleShadowDomCheck = function () {
        this.enableShadowDomCheck = !this.enableShadowDomCheck;
        this.onToggleShadowDomCheck.emit(this.enableShadowDomCheck);
    };
    ProjectBuilderComponent.prototype.killEvent = function (event) {
        event.stopPropagation();
    };
    ProjectBuilderComponent.prototype.validate = function () {
        this.resetStatuses();
        this.manifestStatus = this.validateManifest() ? Status.Complete : Status.Error;
        if (this.manifestStatus === Status.Complete) {
            this.socketSvc.signalCopyProject(this.ptSvc.directoryPath, this.ptSvc.buildDirectoryPath, this.ptSvc.manifest);
        }
    };
    ProjectBuilderComponent.prototype.projectCopySucceeded = function () {
        this.copyStatus = Status.Complete;
        if (this.enableCodeObfuscation) {
            this.socketSvc.signalObfuscate(this.ptSvc.buildDirectoryPath, this.ptSvc.manifest);
        }
        else {
            this.projectObfuscationSucceeded();
        }
    };
    ProjectBuilderComponent.prototype.projectObfuscationSucceeded = function () {
        this.obfuscationStatus = this.enableCodeObfuscation ? Status.Complete : Status.Skipped;
        if (this.enableShadowDomCheck) {
            this.socketSvc.signalShadowDomCheck(this.ptSvc.buildDirectoryPath, this.ptSvc.buildManifest);
        }
        else {
            this.shadowDomStatus = Status.Skipped;
            this.createProjectEntry();
            this.removeScripts();
            this.buildComplete.emit();
        }
    };
    ProjectBuilderComponent.prototype.projectShadowDomCheckSucceeded = function () {
        this.shadowDomStatus = Status.Complete;
        this.createProjectEntry();
        this.removeScripts();
        this.buildComplete.emit();
    };
    ProjectBuilderComponent.prototype.removeScripts = function () {
        var scripts = document.getElementsByClassName("od-script");
        for (var i = 0; i < scripts.length; ++i) {
            scripts[i].remove();
        }
    };
    ProjectBuilderComponent.prototype.createProjectEntry = function () {
        var projectDescriptor = {
            title: this.ptSvc.manifest.title,
            directoryPath: this.ptSvc.directoryPath,
            buildDirectoryPath: this.ptSvc.buildDirectoryPath
        };
        this.socketSvc.signalCreateProject(projectDescriptor);
    };
    ProjectBuilderComponent.prototype.resetStatuses = function () {
        this.manifestStatus = Status.Pending;
        this.copyStatus = Status.Pending;
        this.obfuscationStatus = Status.Pending;
        this.shadowDomStatus = Status.Pending;
        this.errors = new Array();
    };
    ProjectBuilderComponent.prototype.validateManifest = function () {
        var valid = (this.checkApp() &&
            this.checkMicroApp() &&
            this.checkWidgets() &&
            this.checkApiCalls());
        return valid;
    };
    ProjectBuilderComponent.prototype.isString = function (str, errorMessage) {
        var valid = true;
        if (typeof str !== "string" || str.trim().length === 0) {
            valid = false;
            if (errorMessage) {
                this.errors.push(errorMessage);
            }
        }
        return valid;
    };
    ProjectBuilderComponent.prototype.fileExists = function (path, errorMessage) {
        var valid = true;
        if (!this.ptSvc.files.includes(path)) {
            valid = false;
            if (errorMessage) {
                this.errors.push(errorMessage);
            }
        }
        return valid;
    };
    ProjectBuilderComponent.prototype.checkApp = function () {
        return (this.isString(this.manifest.title, "Application Title is missing") &&
            this.isString(this.manifest.iconImage, "Application Icon is missing") &&
            this.fileExists(this.manifest.iconImage, "Invalid file specified for Application Icon") &&
            this.isString(this.manifest.description, "Application description is missing"));
    };
    ProjectBuilderComponent.prototype.checkMicroApp = function () {
        var valid = true;
        if (!this.isString(this.manifest.app.tag) || !this.isString(this.manifest.app.bootstrapModule)) {
            this.manifest.app.tag = null;
            this.manifest.app.bootstrapModule = null;
        }
        else {
            valid = (this.isString(this.manifest.app.tag, "App Tag is missing") &&
                this.isString(this.manifest.app.bootstrapModule, "App Bootstrap Module is missing") &&
                this.fileExists(this.manifest.app.bootstrapModule, "Invalid file specified for the App Bootstrap Module"));
        }
        if (valid) {
            if (this.manifest.app === null && this.manifest.widgets.length === 0) {
                valid = false;
                this.errors.push("No MicroApp or Widgets were defined");
            }
        }
        return valid;
    };
    ProjectBuilderComponent.prototype.checkWidgets = function () {
        var valid = true;
        for (var i = 0; i < this.manifest.widgets.length; ++i) {
            if (!this.isString(this.manifest.widgets[i].title, "Widget Title is missing") ||
                !this.isString(this.manifest.widgets[i].bootstrapModule, "Widget Bootstrap Module is missing") ||
                !this.isString(this.manifest.widgets[i].tag, "Widget Tag is missing") ||
                !this.isString(this.manifest.widgets[i].iconImage, "Widget Icon Image is missing") ||
                !this.fileExists(this.manifest.widgets[i].bootstrapModule, "Invalid file specified for Widget Bootstrap Module") ||
                !this.fileExists(this.manifest.widgets[i].iconImage, "Invalid file specified for Widget Icon Image") ||
                !this.isString(this.manifest.widgets[i].descriptionFull, "Widget full description is missing") ||
                !this.isString(this.manifest.widgets[i].descriptionShort, "Widget short description is missing") ||
                !this.isString(this.manifest.widgets[i].customId, "Widget is missing customId")) {
                valid = false;
                break;
            }
        }
        return valid;
    };
    ProjectBuilderComponent.prototype.checkApiCalls = function () {
        var valid = true;
        for (var i = 0; i < this.manifest.apiCalls.length; ++i) {
            if (!this.isString(this.manifest.apiCalls[i].url, "API Call URL is missing")) {
                valid = false;
                break;
            }
        }
        return valid;
    };
    ProjectBuilderComponent.prototype.subscribeToManifestUpdates = function () {
        var _this = this;
        this.manifestSub = this.ptSvc.manifestSub.subscribe(function (m) {
            _this.manifest = m;
            _this.resetStatuses();
        });
    };
    ProjectBuilderComponent.prototype.subscribeToSlotCopyProject = function () {
        var _this = this;
        this.slotCopyProjectSub = this.socketSvc.slotCopyProjectSub.subscribe(function (buildDirectoryPath) {
            if (buildDirectoryPath.length) {
                _this.ptSvc.buildDirectoryPath = buildDirectoryPath;
                _this.projectCopySucceeded();
            }
            else {
                _this.errors.push("Unable to copy the project directory");
                _this.copyStatus = Status.Error;
                _this.ptSvc.buildDirectoryPath = "";
            }
        });
    };
    ProjectBuilderComponent.prototype.subscribeToSlotObfuscate = function () {
        var _this = this;
        this.slotObfuscateSub = this.socketSvc.slotObfuscate.subscribe(function (manifest) {
            if (manifest) {
                _this.ptSvc.buildManifest = manifest;
                _this.projectObfuscationSucceeded();
                //this.projectShadowDomCheckSucceeded();
            }
            else {
                _this.errors.push("Unable to find matching custom element tags specified in the manifest");
                _this.obfuscationStatus = Status.Error;
                _this.ptSvc.buildManifest = null;
            }
        });
    };
    ProjectBuilderComponent.prototype.subscribeToSlotShadowDomCheck = function () {
        var _this = this;
        this.slotShadowDomCheckSub = this.socketSvc.slotShadowDomCheckSub.subscribe(function (success) {
            if (success) {
                _this.projectShadowDomCheckSucceeded();
            }
            else {
                _this.errors.push("One of your custom elements is not using the Shadow DOM");
                _this.shadowDomStatus = Status.Error;
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('showCancelBtn'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Boolean])
    ], ProjectBuilderComponent.prototype, "showCancelBtn", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ProjectBuilderComponent.prototype, "buildComplete", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ProjectBuilderComponent.prototype, "onToggleCodeObfuscation", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], ProjectBuilderComponent.prototype, "onToggleShadowDomCheck", void 0);
    ProjectBuilderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-project-builder',
            template: __webpack_require__(/*! ./project-builder.component.html */ "./src/app/project-builder/project-builder.component.html"),
            styles: [__webpack_require__(/*! ./project-builder.component.scss */ "./src/app/project-builder/project-builder.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_project_tracker_service__WEBPACK_IMPORTED_MODULE_4__["ProjectTrackerService"],
            _services_socket_service__WEBPACK_IMPORTED_MODULE_3__["SocketService"]])
    ], ProjectBuilderComponent);
    return ProjectBuilderComponent;
}());



/***/ }),

/***/ "./src/app/project-picker/project-picker.component.html":
/*!**************************************************************!*\
  !*** ./src/app/project-picker/project-picker.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"position: relative; margin-top: -20px;\">\n  <h2>Select Project</h2>\n  <button mat-icon-button class=\"position-absolute opacity-half\" style=\"top:-5px;right:0;\" (click)=\"closeClicked()\">\n    <mat-icon>clear</mat-icon>\n  </button>\n\n  <table #projectTable mat-table [dataSource]=\"projects\" style=\"width: 100%\">\n\n    <ng-container matColumnDef=\"title\">\n      <th mat-header-cell *matHeaderCellDef>Title</th>\n      <td mat-cell *matCellDef=\"let project\">\n        <button mat-button (click)=\"openClicked(project)\" matTooltip=\"Open Project\">{{project.title}}</button>\n      </td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"createdAt\">\n      <th mat-header-cell *matHeaderCellDef>Created</th>\n      <td mat-cell *matCellDef=\"let project\">{{project.createdAt | date:'short'}}</td>\n    </ng-container>\n\n    <ng-container matColumnDef=\"actions\">\n      <th mat-header-cell *matHeaderCellDef></th>\n      <td mat-cell *matCellDef=\"let project\" class=\"text-right\">\n        <button mat-icon-button (click)=\"deleteClicked(project)\" class=\"mr-1 opacity-half\" matTooltip=\"Delete Project\"><mat-icon>delete</mat-icon></button>\n        <button mat-icon-button color=\"primary\" (click)=\"openClicked(project)\" matTooltip=\"Open Project\"><mat-icon>remove_red_eye</mat-icon></button>\n      </td>\n    </ng-container>\n\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns\"></tr>\n\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/project-picker/project-picker.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/project-picker/project-picker.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2plY3QtcGlja2VyL3Byb2plY3QtcGlja2VyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/project-picker/project-picker.component.ts":
/*!************************************************************!*\
  !*** ./src/app/project-picker/project-picker.component.ts ***!
  \************************************************************/
/*! exports provided: ProjectPickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectPickerComponent", function() { return ProjectPickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var ProjectPickerComponent = /** @class */ (function () {
    function ProjectPickerComponent() {
        this.displayedColumns = new Array("title", "createdAt", "actions");
        this.projects = new Array();
        this.open = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.delete = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.close = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    ProjectPickerComponent.prototype.ngOnInit = function () {
    };
    ProjectPickerComponent.prototype.openClicked = function (project) {
        this.open.next(project);
    };
    ProjectPickerComponent.prototype.deleteClicked = function (project) {
        this.delete.next(project);
        var index = this.projects.indexOf(project);
        this.projects.splice(index, 1);
        this.projectTable.renderRows();
    };
    ProjectPickerComponent.prototype.closeClicked = function () {
        this.close.next();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ProjectPickerComponent.prototype, "projects", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('projectTable'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTable"])
    ], ProjectPickerComponent.prototype, "projectTable", void 0);
    ProjectPickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-project-picker',
            template: __webpack_require__(/*! ./project-picker.component.html */ "./src/app/project-picker/project-picker.component.html"),
            styles: [__webpack_require__(/*! ./project-picker.component.scss */ "./src/app/project-picker/project-picker.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProjectPickerComponent);
    return ProjectPickerComponent;
}());



/***/ }),

/***/ "./src/app/services/app-launch-request.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/app-launch-request.service.ts ***!
  \********************************************************/
/*! exports provided: AppLaunchRequestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLaunchRequestService", function() { return AppLaunchRequestService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/**
 * @description Service that receives requests to launch apps from widgets
 * @author Steven M. Redman
 */



var AppLaunchRequestService = /** @class */ (function () {
    function AppLaunchRequestService() {
        this.appLaunchSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    AppLaunchRequestService.prototype.requestLaunch = function (request) {
        this.appState = request.data;
        this.appLaunchSubject.next();
    };
    AppLaunchRequestService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AppLaunchRequestService);
    return AppLaunchRequestService;
}());



/***/ }),

/***/ "./src/app/services/http-request-controller.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/http-request-controller.service.ts ***!
  \*************************************************************/
/*! exports provided: HttpRequestControllerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpRequestControllerService", function() { return HttpRequestControllerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _util_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../util/constants */ "./src/app/util/constants.ts");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/index.js");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(uuid__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _http_request_monitor_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./http-request-monitor.service */ "./src/app/services/http-request-monitor.service.ts");
/**
 * @description Creates AJAX calls for third-party apps and widgets per their descriptions. Does not create calls if security checks do not pass.
 * @author Steven M. Redman
 */







var HttpRequestControllerService = /** @class */ (function () {
    function HttpRequestControllerService(http, httpMonitorSvc) {
        this.http = http;
        this.httpMonitorSvc = httpMonitorSvc;
        this.ErrorResponses = {
            UndeclaredInManifest: "HTTP Request was blocked because it was not declared in the manifest",
            UntrustedApp: "Attempted to communicate with a core service using a verb other than 'GET'. This app is not Trusted."
        };
        this.requestCompletionSub = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    HttpRequestControllerService.prototype.send = function (request, app) {
        try {
            if (this.requestIsDeclared(request, app)) {
                var req = this.createRequest(request);
                this.sendRequest(req, request);
            }
            else {
                this.callback(request.onError, this.ErrorResponses.UndeclaredInManifest);
                this.emitRequestCompletion(request, false, this.ErrorResponses.UndeclaredInManifest);
            }
        }
        catch (error) {
            this.callback(request.onError, error);
            this.emitRequestCompletion(request, false, error);
        }
    };
    HttpRequestControllerService.prototype.observeRequestCompletions = function () {
        return this.requestCompletionSub.asObservable();
    };
    HttpRequestControllerService.prototype.cancelRequest = function (reqId) {
        var sub = this.requestTracker.findByKey(reqId);
        if (sub) {
            console.log("HTTP request with ID " + reqId + " aborted");
            sub.unsubscribe();
            this.requestTracker.deleteByKey(reqId);
        }
    };
    HttpRequestControllerService.prototype.requestIsDeclared = function (request, app) {
        var declared = false;
        //let declared: boolean = true;
        if (request.appId && app.apiCalls) {
            declared = this.matchApiCallDescriptor(request, app);
        }
        return declared;
    };
    HttpRequestControllerService.prototype.matchApiCallDescriptor = function (request, app) {
        var matches = false;
        var apiCall = app.apiCalls.find(function (ac) {
            if (request.verb.toLowerCase() === ac.verb.toLowerCase()) {
                var protocolRegExp = new RegExp(/http(s)?:\/\//g);
                var queryRegExp = new RegExp(/\?.*/g);
                var acUrl = ac.url.replace(protocolRegExp, "").replace(queryRegExp, "");
                var reqUrl = request.uri.replace(protocolRegExp, "").replace(queryRegExp, "");
                var splitAc = acUrl.split("/");
                var splitReq_1 = reqUrl.split("/");
                if (splitAc.length === splitReq_1.length) {
                    var combined_1 = new Array();
                    splitAc.forEach(function (part, index) {
                        if (part !== "{$}") {
                            combined_1.push(part);
                        }
                        else if (splitReq_1[index]) {
                            combined_1.push(splitReq_1[index]);
                        }
                    });
                    var combinedStr = combined_1.join("/");
                    return (combinedStr === reqUrl);
                }
            }
            return false;
        });
        if (apiCall) {
            matches = true;
        }
        else {
            console.error("Undeclared API call: " + request.uri);
        }
        return matches;
    };
    HttpRequestControllerService.prototype.createRequest = function (request) {
        var reportProgress = (typeof request.onProgress === "function");
        var headers = this.createHeaders(request.headers);
        var req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpRequest"](request.verb, request.uri, request.body, {
            headers: headers,
            reportProgress: reportProgress,
            responseType: (request.responseType ? request.responseType : request.headers ? this.getResponseType(request.headers) : "json")
        });
        return req;
    };
    HttpRequestControllerService.prototype.getResponseType = function (headers) {
        var contentTypeIndex = headers.findIndex(function (h) { return h.key.toLowerCase() === 'content-type'; });
        if (contentTypeIndex === -1) {
            return "json";
        }
        else {
            var contentType = headers[contentTypeIndex].value;
            if (contentType.match('text')) {
                return "text";
            }
            else if (contentType.match('arraybuffer')) {
                return "arraybuffer";
            }
            else if (contentType.match('blob')) {
                return "blob";
            }
            else {
                return "json";
            }
        }
    };
    HttpRequestControllerService.prototype.createHeaders = function (requestHeaders) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        if (requestHeaders) {
            requestHeaders.forEach(function (h) {
                headers = headers.set(h.key, h.value);
            });
        }
        var signature = uuid__WEBPACK_IMPORTED_MODULE_5__["v4"]();
        headers = headers.set(_util_constants__WEBPACK_IMPORTED_MODULE_4__["HttpSignatureKey"], signature);
        this.httpMonitorSvc.addSignature(signature);
        return headers;
    };
    HttpRequestControllerService.prototype.sendRequest = function (req, request) {
        var _this = this;
        this.http.request(req).subscribe(function (event) {
            if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].Response) {
                console.log("API request completed: " + request.uri);
                console.log(event.body);
                _this.callback(request.onSuccess, event.body);
                _this.emitRequestCompletion(request, true, event.body);
            }
            else if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpEventType"].UploadProgress) {
                _this.callback(request.onProgress, Math.round(event.loaded / event.total) * 100.00);
            }
        }, function (err) {
            console.error("API request failed: " + request.uri);
            _this.callback(request.onError, err);
            _this.emitRequestCompletion(request, false, err);
        });
    };
    HttpRequestControllerService.prototype.callback = function (cb, param) {
        if (typeof cb === "function") {
            cb(param);
        }
    };
    HttpRequestControllerService.prototype.emitRequestCompletion = function (request, succeeded, response) {
        request.succeeded = succeeded;
        request.response = response;
        this.requestCompletionSub.next(request);
    };
    HttpRequestControllerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _http_request_monitor_service__WEBPACK_IMPORTED_MODULE_6__["HttpRequestMonitorService"]])
    ], HttpRequestControllerService);
    return HttpRequestControllerService;
}());



/***/ }),

/***/ "./src/app/services/http-request-monitor.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/http-request-monitor.service.ts ***!
  \**********************************************************/
/*! exports provided: HttpRequestMonitorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpRequestMonitorService", function() { return HttpRequestMonitorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../util/constants */ "./src/app/util/constants.ts");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/index.js");
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(uuid__WEBPACK_IMPORTED_MODULE_3__);
/**
 * @description Monitors all outgoing AJAX requests and sends/aborts them based on signature validity
 * @author Steven M. Redman
 *
 */




var NativeXHR = {
    open: null,
    setRequestHeader: null,
    send: null,
    signatures: [],
    whitelist: [
        "localhost:3000"
    ]
};
var HttpRequestMonitorService = /** @class */ (function () {
    function HttpRequestMonitorService() {
    }
    HttpRequestMonitorService.prototype.start = function () {
        this.monitorOpen();
        this.monitorRequestHeaders();
        this.monitorSend();
    };
    HttpRequestMonitorService.prototype.stop = function () {
        XMLHttpRequest.prototype.open = NativeXHR.open;
        XMLHttpRequest.prototype.setRequestHeader = NativeXHR.setRequestHeader;
        XMLHttpRequest.prototype.send = NativeXHR.send;
        NativeXHR.signatures = [];
    };
    HttpRequestMonitorService.prototype.addSignature = function (signature) {
        NativeXHR.signatures.push(signature);
    };
    HttpRequestMonitorService.prototype.monitorOpen = function () {
        NativeXHR.open = XMLHttpRequest.prototype.open;
        XMLHttpRequest.prototype.open = function () {
            var args = arguments;
            var whitelist = NativeXHR.whitelist.find(function (item) {
                return args[1].toLowerCase().includes(item);
            });
            if (whitelist) {
                var sig = uuid__WEBPACK_IMPORTED_MODULE_3__["v4"]();
                NativeXHR.signatures.push(sig);
                this[_util_constants__WEBPACK_IMPORTED_MODULE_2__["HttpSignatureKey"]] = sig;
            }
            NativeXHR.open.apply(this, arguments);
        };
    };
    HttpRequestMonitorService.prototype.monitorRequestHeaders = function () {
        NativeXHR.setRequestHeader = XMLHttpRequest.prototype.setRequestHeader;
        XMLHttpRequest.prototype.setRequestHeader = function () {
            if (arguments.length === 2 && arguments[0] === _util_constants__WEBPACK_IMPORTED_MODULE_2__["HttpSignatureKey"]) {
                if (NativeXHR.signatures.includes(arguments[1])) {
                    this[_util_constants__WEBPACK_IMPORTED_MODULE_2__["HttpSignatureKey"]] = arguments[1];
                }
            }
            else {
                NativeXHR.setRequestHeader.apply(this, arguments);
            }
        };
    };
    HttpRequestMonitorService.prototype.monitorSend = function () {
        NativeXHR.send = XMLHttpRequest.prototype.send;
        XMLHttpRequest.prototype.send = function () {
            if (this[_util_constants__WEBPACK_IMPORTED_MODULE_2__["HttpSignatureKey"]]) {
                var requestSig_1 = this[_util_constants__WEBPACK_IMPORTED_MODULE_2__["HttpSignatureKey"]];
                var index = NativeXHR.signatures.findIndex(function (sig) { return sig === requestSig_1; });
                if (index > -1) {
                    NativeXHR.signatures.splice(index, 1);
                    NativeXHR.send.apply(this, arguments);
                }
                else {
                    this.abort();
                }
            }
            else {
                this.abort();
            }
        };
    };
    HttpRequestMonitorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HttpRequestMonitorService);
    return HttpRequestMonitorService;
}());



/***/ }),

/***/ "./src/app/services/project-tracker.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/project-tracker.service.ts ***!
  \*****************************************************/
/*! exports provided: ProjectTrackerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectTrackerService", function() { return ProjectTrackerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _app_launch_request_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-launch-request.service */ "./src/app/services/app-launch-request.service.ts");




var ProjectTrackerService = /** @class */ (function () {
    function ProjectTrackerService(launchReqSvc) {
        this.launchReqSvc = launchReqSvc;
        this.manifestSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.filesSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.userStateSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.reloadSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.reloadAppSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.reloadWidgetSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.newProjectSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.projectOpenedSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this._widgetCache = new Map();
        this.files = new Array();
        this.initManifest();
        this.resetUserState();
        this.resetCoreServicesMap();
        this.widgetStates = new Map();
        this.buildDirectoryPath = null;
    }
    Object.defineProperty(ProjectTrackerService.prototype, "manifest", {
        get: function () {
            return this._manifest;
        },
        set: function (m) {
            this._manifest = m;
            this.manifestSub.next(this.manifest);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProjectTrackerService.prototype, "files", {
        get: function () {
            return this._files;
        },
        set: function (f) {
            this._files = f;
            this.filesSub.next(this._files);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProjectTrackerService.prototype, "coreServicesMap", {
        get: function () {
            return this._coreServicesMap;
        },
        set: function (coreServices) {
            this._coreServicesMap = coreServices;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProjectTrackerService.prototype, "userState", {
        get: function () {
            return this._userState;
        },
        set: function (userState) {
            this._userState = userState;
            this.userStateSub.next(userState);
        },
        enumerable: true,
        configurable: true
    });
    ProjectTrackerService.prototype.initManifest = function () {
        this.manifest = {
            title: "Example Title",
            version: "1.0.0",
            iconImage: null,
            permissions: [],
            app: {
                tag: null,
                bootstrapModule: null
            },
            widgets: [],
            apiCalls: []
        };
    };
    ProjectTrackerService.prototype.clearProject = function () {
        this.directoryPath = null;
        this.buildDirectoryPath = null;
        this.buildManifest = null;
        this.initManifest();
        this.files = new Array();
        this.widgetStates.clear();
        this.launchReqSvc.appState = null;
    };
    ProjectTrackerService.prototype.resetUserState = function () {
        this.userState = {
            userId: "abc123",
            realm: "Fake Realm",
            userProfile: {
                username: "fakeuser",
                firstName: "Fake",
                lastName: "User",
                email: "fakeuser@test.com",
                id: "abc123"
            },
            bearerToken: "abcd-1234",
            realmAccess: ["Test Realm Role"],
            resourceAccess: [
                {
                    clientId: "client-id",
                    roles: ["Create", "Read", "Update", "Delete"]
                }
            ]
        };
    };
    ProjectTrackerService.prototype.resetCoreServicesMap = function () {
        this.coreServicesMap = {
            ssoConnection: "http://sso-url.com/",
            userProfileServiceConnection: "http://user-profile-url.com/",
            servicesServiceConnection: "http://services-url.com/",
            vendorsServiceConnection: "http://vendors-url.com/",
            appsServiceConnection: "http://app-url.com/",
            registrationServiceConnection: "http://registration-url.com/",
            feedbackServiceConnection: "http://feedback-url.com/"
        };
    };
    ProjectTrackerService.prototype.subscribeToCache = function (id) {
        if (!this._widgetCache.has(id)) {
            this._widgetCache.set(id, new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({}));
        }
        return this._widgetCache.get(id).asObservable();
    };
    ProjectTrackerService.prototype.readFromCache = function (id) {
        if (this._widgetCache.has(id)) {
            return this._widgetCache.get(id).value;
        }
        return {};
    };
    ProjectTrackerService.prototype.writeToCache = function (id, value) {
        if (this._widgetCache.has(id)) {
            this._widgetCache.get(id).next(value);
        }
        else {
            this._widgetCache.set(id, new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](value));
        }
    };
    ProjectTrackerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_launch_request_service__WEBPACK_IMPORTED_MODULE_3__["AppLaunchRequestService"]])
    ], ProjectTrackerService);
    return ProjectTrackerService;
}());



/***/ }),

/***/ "./src/app/services/socket.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/socket.service.ts ***!
  \********************************************/
/*! exports provided: SocketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SocketService", function() { return SocketService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var SocketService = /** @class */ (function () {
    function SocketService() {
        this.setupIO();
        this.slotListDrivesSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotListFilesSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotReadManifestSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotCopyProjectSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotObfuscate = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotShadowDomCheckSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotListProjectsSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotCreateProjectSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotDeleteProjectSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotListSubDirectoriesSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.slotZipProjectSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    SocketService.prototype.setupIO = function () {
        var _this = this;
        this.socket = io('http://localhost:3000');
        this.socket.on('connect', function () {
            console.log("connected to socket.io!");
        });
        this.socket.on('disconnect', function () {
            console.log('disconnected from socket.io!');
        });
        this.socket.on('slot-listDrives', function (drives) {
            _this.slotListDrivesSub.next(drives);
        });
        this.socket.on('slot-listFiles', function (files) {
            _this.slotListFilesSub.next(files);
        });
        this.socket.on('slot-readManifest', function (manifest) {
            _this.slotReadManifestSub.next(manifest);
        });
        this.socket.on('slot-copyProject', function (newProjectDirectoryPath) {
            _this.slotCopyProjectSub.next(newProjectDirectoryPath);
        });
        this.socket.on('slot-obfuscate', function (manifest) {
            _this.slotObfuscate.next(manifest);
        });
        this.socket.on('slot-shadowDomCheck', function (success) {
            _this.slotShadowDomCheckSub.next(success);
        });
        this.socket.on('slot-listProjects', function (projects) {
            _this.slotListProjectsSub.next(projects);
        });
        this.socket.on('slot-createProject', function (success) {
            _this.slotCreateProjectSub.next(success);
        });
        this.socket.on('slot-deleteProject', function (success) {
            _this.slotDeleteProjectSub.next(success);
        });
        this.socket.on('slot-listSubDirectories', function (directories) {
            _this.slotListSubDirectoriesSub.next(directories);
        });
        this.socket.on('slot-zipProject', function (directoryPath) {
            _this.slotZipProjectSub.next(directoryPath);
        });
    };
    SocketService.prototype.signalListDrives = function () {
        this.socket.emit('signal-listDrives', {});
    };
    SocketService.prototype.signalListFiles = function (directoryPath) {
        this.socket.emit('signal-listFiles', directoryPath);
    };
    SocketService.prototype.signalReadManifest = function (directoryPath) {
        this.socket.emit('signal-readManifest', directoryPath);
    };
    SocketService.prototype.signalCopyProject = function (directoryPath, currentBuildDirectoryPath, manifest) {
        this.socket.emit('signal-copyProject', { directoryPath: directoryPath, currentBuildDirectoryPath: currentBuildDirectoryPath, manifest: manifest });
    };
    SocketService.prototype.signalObfuscate = function (buildDirectoryPath, manifest) {
        this.socket.emit('signal-obfuscate', { directoryPath: buildDirectoryPath, manifest: manifest });
    };
    SocketService.prototype.signalShadowDomCheck = function (buildDirectoryPath, manifest) {
        this.socket.emit('signal-shadowDomCheck', { directoryPath: buildDirectoryPath, manifest: manifest });
    };
    SocketService.prototype.signalListProjects = function () {
        this.socket.emit('signal-listProjects', {});
    };
    SocketService.prototype.signalCreateProject = function (pd) {
        this.socket.emit('signal-createProject', pd);
    };
    SocketService.prototype.signalDeleteProject = function (id) {
        this.socket.emit('signal-deleteProject', id);
    };
    SocketService.prototype.signalListSubDirectories = function (directoryPath) {
        this.socket.emit('signal-listSubDirectories', directoryPath);
    };
    SocketService.prototype.signalZipProject = function (buildDirectoryPath) {
        this.socket.emit('signal-zipProject', buildDirectoryPath);
    };
    SocketService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SocketService);
    return SocketService;
}());



/***/ }),

/***/ "./src/app/services/widget-windows.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/widget-windows.service.ts ***!
  \****************************************************/
/*! exports provided: WidgetWindowsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetWindowsService", function() { return WidgetWindowsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/**
 * @description Service facilitating components to add a widget to the windowing system.
 * @author James Marcu
 */



var WidgetWindowsService = /** @class */ (function () {
    function WidgetWindowsService() {
        this.addWindowSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.removeAppWindowsSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.removeAllSub = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    WidgetWindowsService.prototype.addWindow = function (ap) {
        this.addWindowSub.next(ap);
    };
    WidgetWindowsService.prototype.removeAppWindows = function (appId) {
        this.removeAppWindowsSub.next(appId);
    };
    WidgetWindowsService.prototype.removeAll = function () {
        this.removeAllSub.next();
    };
    WidgetWindowsService.prototype.observeAddWindow = function () {
        return this.addWindowSub.asObservable();
    };
    WidgetWindowsService.prototype.observeAppWindowRemoval = function () {
        return this.removeAppWindowsSub.asObservable();
    };
    WidgetWindowsService.prototype.observeRemoveAll = function () {
        return this.removeAllSub.asObservable();
    };
    WidgetWindowsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], WidgetWindowsService);
    return WidgetWindowsService;
}());



/***/ }),

/***/ "./src/app/sidebar-logo/sidebar-logo.component.html":
/*!**********************************************************!*\
  !*** ./src/app/sidebar-logo/sidebar-logo.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <mat-grid-list cols=\"4\" rowHeight=\"40px\">\n  <mat-grid-tile colspan=\"1\"></mat-grid-tile>\n  <mat-grid-tile colspan=\"3\">\n    <a class=\"appLogo\">\n      <img src=\"assets/images/od360-logo-light.png\" />\n    </a>\n  </mat-grid-tile>\n</mat-grid-list>\n -->\n\n<a matTooltip=\"OpenDash360 Developer Kit\" class=\"appLogo\">\n  <img src=\"assets/images/od360-logo-light.png\" />\n</a>"

/***/ }),

/***/ "./src/app/sidebar-logo/sidebar-logo.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/sidebar-logo/sidebar-logo.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".appLogo {\n  height: auto;\n  width: auto;\n  max-height: 50px;\n  max-width: 140px;\n  display: block; }\n  .appLogo img {\n    display: block;\n    width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2phbWVzL0RvY3VtZW50cy9VS0kvUmVwb3NpdG9yaWVzL0RldmVsb3Blci9vZDM2MC1kZXZlbG9wZXIvc3JjL2FwcC9zaWRlYmFyLWxvZ28vc2lkZWJhci1sb2dvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBWTtFQUNaLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTtFQUxsQjtJQVFRLGNBQWM7SUFDZCxXQUFXLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9zaWRlYmFyLWxvZ28vc2lkZWJhci1sb2dvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFwcExvZ28ge1xuICAgIGhlaWdodDogYXV0bztcbiAgICB3aWR0aDogYXV0bztcbiAgICBtYXgtaGVpZ2h0OiA1MHB4O1xuICAgIG1heC13aWR0aDogMTQwcHg7XG4gICAgZGlzcGxheTogYmxvY2s7XG5cbiAgICBpbWcge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/sidebar-logo/sidebar-logo.component.ts":
/*!********************************************************!*\
  !*** ./src/app/sidebar-logo/sidebar-logo.component.ts ***!
  \********************************************************/
/*! exports provided: SidebarLogoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarLogoComponent", function() { return SidebarLogoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SidebarLogoComponent = /** @class */ (function () {
    function SidebarLogoComponent() {
    }
    SidebarLogoComponent.prototype.ngOnInit = function () {
    };
    SidebarLogoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sidebar-logo',
            template: __webpack_require__(/*! ./sidebar-logo.component.html */ "./src/app/sidebar-logo/sidebar-logo.component.html"),
            styles: [__webpack_require__(/*! ./sidebar-logo.component.scss */ "./src/app/sidebar-logo/sidebar-logo.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SidebarLogoComponent);
    return SidebarLogoComponent;
}());



/***/ }),

/***/ "./src/app/sidebar-menu/sidebar-menu.component.html":
/*!**********************************************************!*\
  !*** ./src/app/sidebar-menu/sidebar-menu.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-nav-list>\n  <a mat-list-item (click)=\"newBuildClicked()\">\n    <mat-icon mat-list-icon>build</mat-icon>\n    <h4 mat-line>Build Project</h4>\n  </a>\n  <a mat-list-item (click)=\"showOpenProjectDialog()\">\n    <mat-icon mat-list-icon>folder_open</mat-icon>\n    <h4 mat-line>Open Project</h4>\n  </a>\n  <a mat-list-item href=\"../../assets/pdfs/OpenDash 360 Developer Guide.pdf\" target=\"_blank\">\n    <mat-icon mat-list-icon>school</mat-icon>\n    <h4 mat-line>Developer Guide</h4>\n  </a>\n  <mat-divider></mat-divider>\n  <a mat-list-item href=\"https://developer.mozilla.org/en-US/docs/Web/Web_Components\" target=\"_blank\">\n    <mat-icon mat-list-icon>link</mat-icon>\n    <h4 mat-line>Web Components</h4>\n  </a>\n\n  <a mat-list-item href=\"https://www.polymer-project.org/\" target=\"_blank\">\n    <mat-icon mat-list-icon>link</mat-icon>\n    <h4 mat-line>Polymer Project</h4>\n  </a>\n  <a mat-list-item href=\"https://reactjs.org/docs/web-components.html\" target=\"_blank\">\n    <mat-icon mat-list-icon>link</mat-icon>\n    <h4 mat-line>React Web Components Guide</h4>\n  </a>\n  <a mat-list-item href=\"https://www.npmjs.com/package/react-web-component\" target=\"_blank\">\n    <mat-icon mat-list-icon>link</mat-icon>\n    <h4 mat-line>React Web Component Library</h4>\n  </a>\n  <a mat-list-item href=\"https://blog.angulartraining.com/tutorial-how-to-create-custom-angular-elements-55aea29d80c5\" target=\"_blank\">\n    <mat-icon mat-list-icon>link</mat-icon>\n    <h4 mat-line>Angular Elements</h4>\n  </a>\n  <a mat-list-item href=\"https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent\" target=\"_blank\">\n    <mat-icon mat-list-icon>link</mat-icon>\n    <h4 mat-line>Custom Events</h4>\n  </a>\n</mat-nav-list>"

/***/ }),

/***/ "./src/app/sidebar-menu/sidebar-menu.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/sidebar-menu/sidebar-menu.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-divider {\n  border-color: rgba(0, 0, 0, 0.1);\n  margin: 5px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2phbWVzL0RvY3VtZW50cy9VS0kvUmVwb3NpdG9yaWVzL0RldmVsb3Blci9vZDM2MC1kZXZlbG9wZXIvc3JjL2FwcC9zaWRlYmFyLW1lbnUvc2lkZWJhci1tZW51LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0NBQXVCO0VBQ3ZCLGFBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NpZGViYXItbWVudS9zaWRlYmFyLW1lbnUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtZGl2aWRlciB7XG4gICAgYm9yZGVyLWNvbG9yOiByZ2JhKCMwMDAsIDAuMSk7XG4gICAgbWFyZ2luOiA1cHggMDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/sidebar-menu/sidebar-menu.component.ts":
/*!********************************************************!*\
  !*** ./src/app/sidebar-menu/sidebar-menu.component.ts ***!
  \********************************************************/
/*! exports provided: SidebarMenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarMenuComponent", function() { return SidebarMenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_socket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/socket.service */ "./src/app/services/socket.service.ts");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _project_picker_project_picker_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../project-picker/project-picker.component */ "./src/app/project-picker/project-picker.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var SidebarMenuComponent = /** @class */ (function () {
    function SidebarMenuComponent(socketSvc, ptSvc, router, dialogSvc) {
        this.socketSvc = socketSvc;
        this.ptSvc = ptSvc;
        this.router = router;
        this.dialogSvc = dialogSvc;
    }
    SidebarMenuComponent.prototype.ngOnInit = function () {
        this.subscribeToSlotListProjects();
    };
    SidebarMenuComponent.prototype.ngOnDestroy = function () {
        this.slotlistProjectsSub.unsubscribe();
    };
    SidebarMenuComponent.prototype.newBuildClicked = function () {
        this.ptSvc.clearProject();
        this.router.navigateByUrl('build');
        this.ptSvc.newProjectSub.next();
    };
    SidebarMenuComponent.prototype.showOpenProjectDialog = function () {
        this.socketSvc.signalListProjects();
    };
    SidebarMenuComponent.prototype.subscribeToSlotListProjects = function () {
        var _this = this;
        this.slotlistProjectsSub = this.socketSvc.slotListProjectsSub.subscribe(function (projects) {
            var dialogRef = _this.dialogSvc.open(_project_picker_project_picker_component__WEBPACK_IMPORTED_MODULE_5__["ProjectPickerComponent"], {
                width: '600px'
            });
            dialogRef.afterOpened().subscribe(function () {
                dialogRef.componentInstance.projects = projects;
                dialogRef.componentInstance.open.subscribe(function (project) {
                    _this.ptSvc.clearProject();
                    _this.ptSvc.directoryPath = project.directoryPath;
                    _this.ptSvc.buildDirectoryPath = project.buildDirectoryPath;
                    _this.router.navigateByUrl('build');
                    _this.ptSvc.projectOpenedSub.next();
                    dialogRef.close();
                });
                dialogRef.componentInstance.delete.subscribe(function (project) {
                    _this.socketSvc.signalDeleteProject(project.id);
                });
                dialogRef.componentInstance.close.subscribe(function () {
                    dialogRef.close();
                });
            });
        });
    };
    SidebarMenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sidebar-menu',
            template: __webpack_require__(/*! ./sidebar-menu.component.html */ "./src/app/sidebar-menu/sidebar-menu.component.html"),
            styles: [__webpack_require__(/*! ./sidebar-menu.component.scss */ "./src/app/sidebar-menu/sidebar-menu.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_socket_service__WEBPACK_IMPORTED_MODULE_2__["SocketService"],
            _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_3__["ProjectTrackerService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"]])
    ], SidebarMenuComponent);
    return SidebarMenuComponent;
}());



/***/ }),

/***/ "./src/app/ui-directory-picker/ui-directory-picker.component.html":
/*!************************************************************************!*\
  !*** ./src/app/ui-directory-picker/ui-directory-picker.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"position: relative; height: 600px; margin-top:-20px; padding: 0 20px;\">\n    <h2>Select Directory</h2>\n    <button mat-icon-button class=\"position-absolute opacity-half\" style=\"top: -5px; right: 0;\" (click)=\"closeClicked()\">\n      <mat-icon>clear</mat-icon>\n    </button>\n    <div class=\"mb-2\">\n      <mat-chip-list>\n        <mat-chip *ngFor=\"let dir of directoryTree; let i = index\" (click)=\"navigate(i)\">\n          {{dir}}\n        </mat-chip>\n      </mat-chip-list>\n    </div>\n    <div class=\"directoryNav\">\n      <mat-list>\n        <mat-list-item *ngFor=\"let drive of drives\" (click)=\"driveClicked(drive)\">\n          <h4 mat-line>{{drive}}</h4>\n        </mat-list-item>\n      </mat-list>\n    </div>\n    <div class=\"directoryNav\">\n      <mat-list>\n        <mat-list-item *ngFor=\"let dir of directories\" (click)=\"listSubDirectories(dir)\">\n          <mat-icon mat-list-icon class=\"opacity-half\">folder</mat-icon>\n          <h4 mat-line>{{dir}}</h4>\n        </mat-list-item>\n      </mat-list>\n    </div>\n    <div style=\"right:0;bottom:10px;\" class=\"position-absolute\">\n      <button mat-raised-button color=\"primary\" (click)=\"chooseDirectoryClicked()\">Choose Directory</button>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/ui-directory-picker/ui-directory-picker.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/ui-directory-picker/ui-directory-picker.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-chip {\n  cursor: pointer; }\n\nmat-list-item {\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none; }\n\nmat-list-item:hover {\n    cursor: pointer;\n    background: #eee;\n    border-radius: 5px; }\n\n.directoryNav {\n  display: inline-block;\n  width: 50%;\n  box-sizing: border-box;\n  float: left;\n  border-right: 1px solid #ccc;\n  height: calc(100% - 135px);\n  overflow-y: scroll; }\n\nmat-list:empty {\n  display: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2phbWVzL0RvY3VtZW50cy9VS0kvUmVwb3NpdG9yaWVzL0RldmVsb3Blci9vZDM2MC1kZXZlbG9wZXIvc3JjL2FwcC91aS1kaXJlY3RvcnktcGlja2VyL3VpLWRpcmVjdG9yeS1waWNrZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFlLEVBQUE7O0FBR25CO0VBQ0kseUJBQWlCO0tBQWpCLHNCQUFpQjtNQUFqQixxQkFBaUI7VUFBakIsaUJBQWlCLEVBQUE7O0FBRHJCO0lBSVEsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQixrQkFBa0IsRUFBQTs7QUFJMUI7RUFDSSxxQkFBcUI7RUFDckIsVUFBVTtFQUNWLHNCQUFzQjtFQUN0QixXQUFXO0VBQ1gsNEJBQTRCO0VBQzVCLDBCQUEwQjtFQUMxQixrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxhQUFhLEVBQUEiLCJmaWxlIjoic3JjL2FwcC91aS1kaXJlY3RvcnktcGlja2VyL3VpLWRpcmVjdG9yeS1waWNrZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJtYXQtY2hpcCB7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG5tYXQtbGlzdC1pdGVtIHtcbiAgICB1c2VyLXNlbGVjdDogbm9uZTtcblxuICAgICY6aG92ZXIge1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQ6ICNlZWU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICB9XG59XG5cbi5kaXJlY3RvcnlOYXYge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogNTAlO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2NjYztcbiAgICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDEzNXB4KTtcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XG59XG5cbm1hdC1saXN0OmVtcHR5IHtcbiAgICBkaXNwbGF5OiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/ui-directory-picker/ui-directory-picker.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/ui-directory-picker/ui-directory-picker.component.ts ***!
  \**********************************************************************/
/*! exports provided: UiDirectoryPickerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UiDirectoryPickerComponent", function() { return UiDirectoryPickerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_socket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/socket.service */ "./src/app/services/socket.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var UiDirectoryPickerComponent = /** @class */ (function () {
    function UiDirectoryPickerComponent(socketSvc) {
        this.socketSvc = socketSvc;
        this.directories = new Array();
        this.drives = new Array();
        this.directoryTree = new Array();
        this.directorySelectedSub = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.closedSub = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
    }
    UiDirectoryPickerComponent.prototype.ngOnInit = function () {
        this.subscribeToListSubDirectories();
        this.subscribeToListDrives();
        //this.listSubDirectories("/");
        this.listDrives();
    };
    UiDirectoryPickerComponent.prototype.ngOnDestroy = function () {
        if (this.slotListSubDirectoriesSub) {
            this.slotListSubDirectoriesSub.unsubscribe();
        }
        if (this.slotListDrivesSub) {
            this.slotListDrivesSub.unsubscribe();
        }
    };
    UiDirectoryPickerComponent.prototype.driveClicked = function (drive) {
        this.directoryTree = new Array();
        this.directories = new Array();
        this.listSubDirectories(drive);
    };
    UiDirectoryPickerComponent.prototype.listSubDirectories = function (selectedDir) {
        if (selectedDir === void 0) { selectedDir = null; }
        if (selectedDir) {
            this.directoryTree.push(selectedDir);
        }
        var directoryPath = this.createDirectoryPath();
        this.socketSvc.signalListSubDirectories(directoryPath);
    };
    UiDirectoryPickerComponent.prototype.navigate = function (index) {
        this.directoryTree.splice(index + 1);
        this.listSubDirectories();
    };
    UiDirectoryPickerComponent.prototype.chooseDirectoryClicked = function () {
        this.directorySelectedSub.next(this.createDirectoryPath());
    };
    UiDirectoryPickerComponent.prototype.closeClicked = function () {
        this.closedSub.next();
    };
    UiDirectoryPickerComponent.prototype.listDrives = function () {
        this.socketSvc.signalListDrives();
    };
    UiDirectoryPickerComponent.prototype.subscribeToListSubDirectories = function () {
        var _this = this;
        this.slotListSubDirectoriesSub = this.socketSvc.slotListSubDirectoriesSub.subscribe(function (directories) {
            _this.directories = directories;
        });
    };
    UiDirectoryPickerComponent.prototype.subscribeToListDrives = function () {
        var _this = this;
        this.slotListDrivesSub = this.socketSvc.slotListDrivesSub.subscribe(function (drives) {
            _this.drives = drives;
        });
    };
    UiDirectoryPickerComponent.prototype.createDirectoryPath = function () {
        var joined = this.directoryTree.join("/");
        joined = joined.replace(/\/\/|\\\//g, '/');
        return joined;
    };
    UiDirectoryPickerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ui-directory-picker',
            template: __webpack_require__(/*! ./ui-directory-picker.component.html */ "./src/app/ui-directory-picker/ui-directory-picker.component.html"),
            styles: [__webpack_require__(/*! ./ui-directory-picker.component.scss */ "./src/app/ui-directory-picker/ui-directory-picker.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_socket_service__WEBPACK_IMPORTED_MODULE_2__["SocketService"]])
    ], UiDirectoryPickerComponent);
    return UiDirectoryPickerComponent;
}());



/***/ }),

/***/ "./src/app/user-state-editor/user-state-editor.component.html":
/*!********************************************************************!*\
  !*** ./src/app/user-state-editor/user-state-editor.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"position:relative;margin-top:-20px;\">\n  <h2>User State Editor</h2>\n  <button mat-icon-button class=\"position-absolute opacity-half\" style=\"top:-5px;right:0;\" (click)=\"onCancel()\">\n    <mat-icon>clear</mat-icon>\n  </button>\n  <div mat-dialog-content>\n    <textarea #userStateEditor spellcheck=\"false\" style=\"width: 100%; box-sizing: border-box; height: 400px; resize: none\">\n    {{data.userState | json}}\n    </textarea>\n  </div>\n  <div mat-dialog-actions>\n    <button mat-raised-button color=\"primary\" (click)=\"onSave()\" cdkFocusInitial matTooltip=\"Save User State\">Save</button>\n    <button mat-button (click)=\"onCancel()\">Cancel</button>\n  </div>\n  <div mat-dialog-footer>\n    <span *ngIf=\"jsonError\">Invalid JSON</span>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/user-state-editor/user-state-editor.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/user-state-editor/user-state-editor.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXItc3RhdGUtZWRpdG9yL3VzZXItc3RhdGUtZWRpdG9yLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/user-state-editor/user-state-editor.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/user-state-editor/user-state-editor.component.ts ***!
  \******************************************************************/
/*! exports provided: UserStateEditorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserStateEditorComponent", function() { return UserStateEditorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");



var UserStateEditorComponent = /** @class */ (function () {
    function UserStateEditorComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.jsonError = false;
    }
    UserStateEditorComponent.prototype.ngOnInit = function () {
    };
    UserStateEditorComponent.prototype.onSave = function () {
        try {
            var newUserState = JSON.parse(this.userStateEditor.nativeElement.value);
            this.dialogRef.close(Object.assign({}, newUserState));
        }
        catch (err) {
            this.jsonError = true;
        }
    };
    UserStateEditorComponent.prototype.onCancel = function () {
        this.dialogRef.close(null);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('userStateEditor'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], UserStateEditorComponent.prototype, "userStateEditor", void 0);
    UserStateEditorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-state-editor',
            template: __webpack_require__(/*! ./user-state-editor.component.html */ "./src/app/user-state-editor/user-state-editor.component.html"),
            styles: [__webpack_require__(/*! ./user-state-editor.component.scss */ "./src/app/user-state-editor/user-state-editor.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object])
    ], UserStateEditorComponent);
    return UserStateEditorComponent;
}());



/***/ }),

/***/ "./src/app/util/cloner.ts":
/*!********************************!*\
  !*** ./src/app/util/cloner.ts ***!
  \********************************/
/*! exports provided: Cloner */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cloner", function() { return Cloner; });
/**
 * @description Utility class containing methods for cloning arrays and objects
 * @author Steven M. Redman
 */
var Cloner = /** @class */ (function () {
    function Cloner() {
    }
    Cloner.cloneObjectArray = function (items) {
        var clones = new Array();
        items.forEach(function (item) {
            clones.push(Cloner.cloneObject(item));
        });
        return clones;
    };
    Cloner.cloneObject = function (item) {
        return JSON.parse(JSON.stringify(item));
    };
    return Cloner;
}());



/***/ }),

/***/ "./src/app/util/constants.ts":
/*!***********************************!*\
  !*** ./src/app/util/constants.ts ***!
  \***********************************/
/*! exports provided: ConnectionStatus, CommonLocalStorageKeys, HttpSignatureKey, FileServerURL, DefaultAppIcon, CustomEventListeners, AppWidgetAttributes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionStatus", function() { return ConnectionStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonLocalStorageKeys", function() { return CommonLocalStorageKeys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpSignatureKey", function() { return HttpSignatureKey; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileServerURL", function() { return FileServerURL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DefaultAppIcon", function() { return DefaultAppIcon; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomEventListeners", function() { return CustomEventListeners; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppWidgetAttributes", function() { return AppWidgetAttributes; });
var ConnectionStatus;
(function (ConnectionStatus) {
    ConnectionStatus[ConnectionStatus["Pending"] = 1] = "Pending";
    ConnectionStatus[ConnectionStatus["Success"] = 2] = "Success";
    ConnectionStatus[ConnectionStatus["Failed"] = 3] = "Failed";
})(ConnectionStatus || (ConnectionStatus = {}));
;
var CommonLocalStorageKeys;
(function (CommonLocalStorageKeys) {
    CommonLocalStorageKeys["RedirectURI"] = "redirectURI";
})(CommonLocalStorageKeys || (CommonLocalStorageKeys = {}));
;
var HttpSignatureKey = "od360-request-signature";
var FileServerURL = "http://localhost:3000/files/?filePath=";
var DefaultAppIcon = "/assets/images/default-microapp-ico-50x50.png";
var CustomEventListeners = {
    OnHttpRequestEvent: "onHttpRequest",
    OnStateChangeEvent: "onStateChange",
    OnAppLaunchRequestEvent: "onAppLaunchRequest",
    OnSharedWidgetCacheWrite: "onSharedWidgetCacheWrite",
    OnHttpAbortEvent: "onAbortHttpRequest",
    OnUserStateCallback: "onUserStateCallback",
    OnResizeCallback: "onResizeCallback",
    OnWidgetCacheCallback: "onWidgetCacheCallback",
    OnInitCallback: "onInitCallback"
};
var AppWidgetAttributes = {
    IsInit: "isinit"
};


/***/ }),

/***/ "./src/app/util/file-mapper.ts":
/*!*************************************!*\
  !*** ./src/app/util/file-mapper.ts ***!
  \*************************************/
/*! exports provided: FileMapper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileMapper", function() { return FileMapper; });
var FileMapper = /** @class */ (function () {
    function FileMapper() {
    }
    FileMapper.mapImageFiles = function (files) {
        var accepted = ["png", "jpg", "jpeg", "svg"];
        return files.filter(function (file) {
            var split = file.split('.');
            if (split.length >= 2) {
                return accepted.includes(split[split.length - 1].toLowerCase());
            }
            return false;
        });
    };
    FileMapper.mapJSFiles = function (files) {
        return files.filter(function (file) {
            var split = file.split('.');
            if (split.length >= 2) {
                return split[split.length - 1].toLowerCase() === "js";
            }
            return false;
        });
    };
    return FileMapper;
}());



/***/ }),

/***/ "./src/app/util/text-transform.ts":
/*!****************************************!*\
  !*** ./src/app/util/text-transform.ts ***!
  \****************************************/
/*! exports provided: TextTransform */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TextTransform", function() { return TextTransform; });
var TextTransform = /** @class */ (function () {
    function TextTransform() {
    }
    TextTransform.transformCustomElementTag = function (tag) {
        return tag.trim().replace(/[^A-Za-z0-9\-]/g, "").toLowerCase();
    };
    return TextTransform;
}());



/***/ }),

/***/ "./src/app/widget-builder/widget-builder.component.html":
/*!**************************************************************!*\
  !*** ./src/app/widget-builder/widget-builder.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-grid-list cols=\"1\" rowHeight=\"65px\">\n  <mat-grid-tile>\n    <h2>Configure Widgets</h2>\n    <button mat-icon-button color=\"secondary\" class=\"ml-2\" (click)=\"addWidget()\" matTooltip=\"Add Additional Widget\">\n      <mat-icon>add</mat-icon>\n    </button>\n  </mat-grid-tile>\n</mat-grid-list>\n\n<mat-card *ngFor=\"let widget of widgets; let i = index\">\n  <mat-list>\n    <mat-list-item>\n      <button mat-icon-button matTooltip=\"Remove Widget\" (click)=\"removeWidget(i)\" class=\"closeButton opacity-half\">\n        <mat-icon>clear</mat-icon>\n      </button>\n    </mat-list-item>\n\n    <mat-list-item class=\"mb-4\">\n      <mat-form-field>\n        <input matInput placeholder=\"Widget Title\" [(ngModel)]=\"widget.title\" required>\n      </mat-form-field>\n    </mat-list-item>\n\n    <mat-list-item class=\"mb-4\">\n      <mat-form-field>\n        <textarea matInput maxlength=\"128\" [(ngModel)]=\"widget.descriptionShort\" placeholder=\"Short Description\"\n          matTextareaAutosize matAutosizeMinRows=2 matAutosizeMaxRows=2 required></textarea>\n      </mat-form-field>\n    </mat-list-item>\n\n    <mat-list-item class=\"mb-4\">\n      <mat-form-field>\n        <textarea matInput maxlength=\"1000\" [(ngModel)]=\"widget.descriptionFull\" placeholder=\"Full Description\"\n          matTextareaAutosize matAutosizeMinRows=2 matAutosizeMaxRows=2 required></textarea>\n      </mat-form-field>\n    </mat-list-item>\n\n    <mat-list-item class=\"mb-4\">\n      <mat-form-field>\n        <input matInput placeholder=\"Custom Element Tag\" [ngModel]=\"widget.tag\"\n          (keyup)=\"widgetTagChanged($event, widget)\">\n      </mat-form-field>\n    </mat-list-item>\n\n    <mat-list-item class=\"mb-4\">\n      <mat-form-field>\n        <mat-label>Bootstrap Module</mat-label>\n        <mat-select [(value)]=\"widget.bootstrapModule\" required>\n          <mat-option *ngFor=\"let file of jsFiles\" [value]=\"file\">\n            {{file}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </mat-list-item>\n\n    <mat-list-item class=\"mb-4\">\n      <mat-form-field>\n        <mat-label>Widget Icon Image</mat-label>\n        <mat-select [(value)]=\"widget.iconImage\" required>\n          <mat-option *ngFor=\"let file of imageFiles\" [value]=\"file\">\n            {{file}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </mat-list-item>\n\n    <mat-list-item class=\"mb-4\">\n      <mat-form-field>\n        <input matInput placeholder=\"Custom ID\" [(ngModel)]=\"widget.customId\" required>\n      </mat-form-field>\n    </mat-list-item>\n  </mat-list>\n</mat-card>\n\n<div>\n  <button mat-button matStepperPrevious class=\"mr-1\">Back</button>\n  <button mat-raised-button color=\"primary\" (click)=\"submit()\">Next</button>\n</div>"

/***/ }),

/***/ "./src/app/widget-builder/widget-builder.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/widget-builder/widget-builder.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-card {\n  display: inline-block;\n  width: calc(33% - 80px);\n  margin: 0 40px 40px 0; }\n\n.closeButton {\n  position: absolute;\n  top: 0;\n  right: 8px; }\n\nmat-card mat-form-field {\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2phbWVzL0RvY3VtZW50cy9VS0kvUmVwb3NpdG9yaWVzL0RldmVsb3Blci9vZDM2MC1kZXZlbG9wZXIvc3JjL2FwcC93aWRnZXQtYnVpbGRlci93aWRnZXQtYnVpbGRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFxQjtFQUNyQix1QkFBdUI7RUFDdkIscUJBQXFCLEVBQUE7O0FBR3pCO0VBQ0ksa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixVQUFVLEVBQUE7O0FBR2Q7RUFDSSxXQUFXLEVBQUEiLCJmaWxlIjoic3JjL2FwcC93aWRnZXQtYnVpbGRlci93aWRnZXQtYnVpbGRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtY2FyZCB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiBjYWxjKDMzJSAtIDgwcHgpO1xuICAgIG1hcmdpbjogMCA0MHB4IDQwcHggMDtcbn1cblxuLmNsb3NlQnV0dG9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIHJpZ2h0OiA4cHg7XG59XG5cbm1hdC1jYXJkIG1hdC1mb3JtLWZpZWxkIHtcbiAgICB3aWR0aDogMTAwJTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/widget-builder/widget-builder.component.ts":
/*!************************************************************!*\
  !*** ./src/app/widget-builder/widget-builder.component.ts ***!
  \************************************************************/
/*! exports provided: WidgetBuilderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetBuilderComponent", function() { return WidgetBuilderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _util_file_mapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../util/file-mapper */ "./src/app/util/file-mapper.ts");
/* harmony import */ var _util_text_transform__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../util/text-transform */ "./src/app/util/text-transform.ts");
/* harmony import */ var _services_project_tracker_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/project-tracker.service */ "./src/app/services/project-tracker.service.ts");





var WidgetBuilderComponent = /** @class */ (function () {
    function WidgetBuilderComponent(ptSvc) {
        this.ptSvc = ptSvc;
        this.widgets = new Array();
        this.submitted = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    WidgetBuilderComponent.prototype.ngOnInit = function () {
        this.subscribeToFileUpdates();
        this.subscribeToManifestUpdates();
    };
    WidgetBuilderComponent.prototype.ngOnDestroy = function () {
        this.filesSub.unsubscribe();
        this.manifestSub.unsubscribe();
    };
    WidgetBuilderComponent.prototype.addWidget = function () {
        this.widgets.push({
            tag: null,
            title: null,
            bootstrapModule: null,
            iconImage: null,
            descriptionFull: null,
            descriptionShort: null,
            customId: null
        });
    };
    WidgetBuilderComponent.prototype.removeWidget = function (index) {
        this.widgets.splice(index, 1);
    };
    WidgetBuilderComponent.prototype.widgetTagChanged = function ($event, widget) {
        var value = $event.currentTarget.value;
        if (value.length) {
            widget.tag = _util_text_transform__WEBPACK_IMPORTED_MODULE_3__["TextTransform"].transformCustomElementTag(value);
        }
        else {
            widget.tag = null;
        }
    };
    WidgetBuilderComponent.prototype.submit = function () {
        this.submitted.emit();
    };
    WidgetBuilderComponent.prototype.subscribeToFileUpdates = function () {
        var _this = this;
        this.filesSub = this.ptSvc.filesSub.subscribe(function (files) {
            _this.imageFiles = _util_file_mapper__WEBPACK_IMPORTED_MODULE_2__["FileMapper"].mapImageFiles(files);
            _this.jsFiles = _util_file_mapper__WEBPACK_IMPORTED_MODULE_2__["FileMapper"].mapJSFiles(files);
        });
    };
    WidgetBuilderComponent.prototype.subscribeToManifestUpdates = function () {
        var _this = this;
        this.manifestSub = this.ptSvc.manifestSub.subscribe(function (manifest) {
            _this.widgets = manifest.widgets;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], WidgetBuilderComponent.prototype, "submitted", void 0);
    WidgetBuilderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-widget-builder',
            template: __webpack_require__(/*! ./widget-builder.component.html */ "./src/app/widget-builder/widget-builder.component.html"),
            styles: [__webpack_require__(/*! ./widget-builder.component.scss */ "./src/app/widget-builder/widget-builder.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_project_tracker_service__WEBPACK_IMPORTED_MODULE_4__["ProjectTrackerService"]])
    ], WidgetBuilderComponent);
    return WidgetBuilderComponent;
}());



/***/ }),

/***/ "./src/app/widget-windows/widget-windows.component.html":
/*!**************************************************************!*\
  !*** ./src/app/widget-windows/widget-windows.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- FLOATING WIDGETS -->\n<div style='min-height: 200px; min-width: 210px;' ngDraggable [handle]='wwDragHandle' ngResizable rzHandles='all' (rzStop)=\"resize(i)\" *ngFor=\"let model of models; index as i\">\n  <div #wwDragHandle style='position: absolute; width: calc(100% - 75px); height: 26px'></div>\n  <app-widget-renderer  \n    style='top: 0; width: 100%; height: 100%; z-index: -1;'\n    [ngClass]=\"{'floating-widget-window': true, 'hidden-widget-window': (model.docked || model.maximized)}\" \n    [app]='model.aww.app'\n    [widget]='model.aww.widget'\n    \n    [format]='rendererFormatFloating'\n    [resize]='model.resize'\n    (leftBtnClick)=\"maximize(i)\"\n    (middleBtnClick)=\"toggleDocked(i)\"\n    (rightBtnClick)=\"removeWindow(i)\"\n    (stateChanged)=\"stateChanged($event, i)\">\n  </app-widget-renderer>\n</div>\n\n<!-- DOCKED WIDGETS -->\n<div class='widget-window-dock'>\n  <div *ngFor=\"let model of models; index as i\" class='docked-widget-window'>\n    <app-widget-renderer *ngIf=\"model.docked\"\n      [minimized]=\"'true'\"\n      [app]='model.aww.app'\n      [widget]='model.aww.widget'\n      [format]='rendererFormatDocked'\n      (leftBtnClick)=\"maximize(i)\"\n      (middleBtnClick)=\"toggleDocked(i)\"\n      (rightBtnClick)=\"removeWindow(i)\"\n    ></app-widget-renderer>\n  </div>\n</div>\n\n<div *ngFor=\"let model of models; index as i\">\n  <app-widget-renderer *ngIf=\"model.maximized\" class=\"widget-maximized\"\n    [app]='model.aww.app'\n    [widget]='model.aww.widget'\n    [format]='rendererFormatMaximized'\n    (leftBtnClick)='popoutMaximizedWidget(i)'\n    (middleBtnClick)='dockMaximizedWidget(i)'\n    (rightBtnClick)='minimize(i); removeWindow(i)'\n    (stateChanged)='stateChanged($event, i)'\n  ></app-widget-renderer>\n</div>"

/***/ }),

/***/ "./src/app/widget-windows/widget-windows.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/widget-windows/widget-windows.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dpZGdldC13aW5kb3dzL3dpZGdldC13aW5kb3dzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/widget-windows/widget-windows.component.ts":
/*!************************************************************!*\
  !*** ./src/app/widget-windows/widget-windows.component.ts ***!
  \************************************************************/
/*! exports provided: WidgetWindowsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetWindowsComponent", function() { return WidgetWindowsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var src_app_services_widget_windows_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/widget-windows.service */ "./src/app/services/widget-windows.service.ts");




var WidgetWindowsComponent = /** @class */ (function () {
    function WidgetWindowsComponent(widgetWindowsSvc) {
        this.widgetWindowsSvc = widgetWindowsSvc;
        this.models = [];
        this.rendererFormatFloating = {
            cardClass: 'gridster-card-view-mode', widgetBodyClass: '',
            leftBtn: { class: "", icon: "crop_square", disabled: false },
            middleBtn: { class: "", icon: "remove", disabled: false },
            rightBtn: { class: "", icon: "clear", disabled: false }
        };
        this.rendererFormatDocked = {
            cardClass: 'gridster-card-view-mode', widgetBodyClass: '',
            leftBtn: { class: "", icon: "crop_square", disabled: false },
            middleBtn: { class: "", icon: "filter_none", disabled: false },
            rightBtn: { class: "", icon: "clear", disabled: false }
        };
        this.rendererFormatMaximized = {
            cardClass: 'gridster-card-view-mode', widgetBodyClass: '',
            leftBtn: { class: "", icon: "filter_none", disabled: false },
            middleBtn: { class: "", icon: "remove", disabled: false },
            rightBtn: { class: "", icon: "clear", disabled: false }
        };
    }
    WidgetWindowsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.widgetWindowsSvc.observeAddWindow().subscribe(function (modelPair) { return _this.addWindow(modelPair); });
        this.widgetWindowsSvc.observeRemoveAll().subscribe(function () { return _this.models = []; });
    };
    WidgetWindowsComponent.prototype.addWindow = function (modelPair) {
        this.models.push({
            aww: modelPair,
            docked: false,
            maximized: false,
            resize: new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]()
        });
    };
    WidgetWindowsComponent.prototype.removeWindow = function (index) {
        this.models.splice(index, 1);
    };
    WidgetWindowsComponent.prototype.removeWindowsByAppId = function (appId) {
        this.models = this.models.filter(function (model) { return model.aww.app.docId !== appId; });
    };
    WidgetWindowsComponent.prototype.toggleDocked = function (index) {
        this.models[index].docked = !this.models[index].docked;
    };
    WidgetWindowsComponent.prototype.maximize = function (index) {
        this.models[index].maximized = true;
    };
    WidgetWindowsComponent.prototype.popoutMaximizedWidget = function (index) {
        this.models[index].docked = false;
        this.minimize(index);
    };
    WidgetWindowsComponent.prototype.dockMaximizedWidget = function (index) {
        this.models[index].docked = true;
        this.minimize(index);
    };
    WidgetWindowsComponent.prototype.stateChanged = function (state, index) {
        this.models[index].aww.widget.state = state;
    };
    WidgetWindowsComponent.prototype.minimize = function (index) {
        this.models[index].maximized = false;
    };
    WidgetWindowsComponent.prototype.resize = function (index) {
        this.models[index].resize.next();
    };
    WidgetWindowsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-widget-windows',
            template: __webpack_require__(/*! ./widget-windows.component.html */ "./src/app/widget-windows/widget-windows.component.html"),
            styles: [__webpack_require__(/*! ./widget-windows.component.scss */ "./src/app/widget-windows/widget-windows.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_widget_windows_service__WEBPACK_IMPORTED_MODULE_3__["WidgetWindowsService"]])
    ], WidgetWindowsComponent);
    return WidgetWindowsComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/james/Documents/UKI/Repositories/Developer/od360-developer/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map