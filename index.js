'use strict';

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const FilesService = require('./files.service');
const fs = require('fs-extra');
const cors = require('cors');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });
const bodyParser = require('body-parser');
const path = require('path');

app.use(cors());
app.use(bodyParser.json());

io.on('connection', (socket) => {

    socket.on('signal-listDrives', () => {
        FilesService.listDrives()
        .then((drives) => {
            socket.emit('slot-listDrives', drives);
        })
        .catch((err) => {
            console.log(err);
        });
    });

    socket.on('signal-listFiles', (directoryPath) => {
        FilesService.listFiles(directoryPath)
            .then((files) => {
                socket.emit('slot-listFiles', files);
            });
    });

    socket.on('signal-listProjects', async () => {
        const projects = await FilesService.listProjects();
        socket.emit('slot-listProjects', projects);
    });

    socket.on('signal-createProject', async (projectInfo) => {
        const success = await FilesService.createProject(projectInfo);
        socket.emit('slot-createProject', success);
    });

    socket.on('signal-deleteProject', async (id) => {
        const success = await FilesService.deleteProject(id);
        socket.emit('slot-deleteProject', success);
    });

    socket.on('signal-readManifest', async (directoryPath) => {
        const manifest = await FilesService.readManifest(directoryPath);
        socket.emit('slot-readManifest', manifest);
    });

    socket.on('signal-copyProject', async (dirInfo) => {
        const success = await FilesService.copyProject(dirInfo.directoryPath, dirInfo.currentBuildDirectoryPath, dirInfo.manifest);
        socket.emit('slot-copyProject', success);
    });

    socket.on('signal-obfuscate', async (info) => {
        const newManifest = await FilesService.obfuscate(info.manifest, info.directoryPath);
        socket.emit('slot-obfuscate', newManifest);
    });

    socket.on('signal-shadowDomCheck', async (info) => {
        const success = await FilesService.checkShadowDom(info.directoryPath, info.manifest);
        socket.emit('slot-shadowDomCheck', success);
    });

    socket.on('signal-listSubDirectories', (directoryPath) => {
        FilesService.listSubDirectories(directoryPath)
        .then((results) => {
            socket.emit('slot-listSubDirectories', results);
        });
    });

    socket.on('signal-zipProject', (buildDirectoryPath) => {
        FilesService.zipProject(buildDirectoryPath)
        .then((outputFilePath) => {
            socket.emit('slot-zipProject', outputFilePath);
        });
    });

});

app.get('/files', async (req, res) => {
    const filePath = FilesService.getOSPath(req.query.filePath);
    const basename = path.basename(filePath);
    const root = filePath.replace(basename, "");
    const exists = await fs.exists(filePath);
    if (exists) {
        res.sendFile(basename, { root: root });
    }
    else {
        res.status(404).json({ message: "Resource not found" });
    }
});

app.use(express.static(path.join(__dirname, 'app', 'dist', 'od360-developer')));
app.get('/', (req, res) => {
    res.sendfile('./app/dist/od360-developer/index.html');
});

server.listen(3000, () => {
    console.log('Listening on port 3000...');
});